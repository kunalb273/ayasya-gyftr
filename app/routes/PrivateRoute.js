import React from 'react';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
// import { push } from 'react-router-redux';

const UserRoute = ({ isAuthenticated, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated ? <Component {...props} /> : <Redirect to="/auth/login" />}
  />
);

UserRoute.propTypes = {
  component: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

function mapStateToProps() {
  const user = Cookies.get('Authentication');
  return {
    isAuthenticated: !!user,
  };
}

export default connect(mapStateToProps)(UserRoute);
