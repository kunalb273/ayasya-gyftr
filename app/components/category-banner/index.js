import React, { Component } from 'react';
import CategroyThumb from '../../styles/images/image_fashion.jpg';

/* eslint-disable*/
export default class CategoryBanner extends Component {
  render() {
    return (
      <div className="inner-images">
        <img className="d-block w-100" src={CategroyThumb} alt="" />
        <div className="inner-images-container">
          <div className="container">
            <h3>Category Name</h3>
          </div>
        </div>
      </div>
    );
  }
}
