import React, { Component } from 'react';
import PropTypes from 'prop-types';
import rupeeIcon from '../../styles/images/inr.svg';

class BrandProduct extends Component {
  componentWillMount() {
    this.calculateTotal();
  }

  total=0

  calculateTotal() {
    this.props.brandProductdata.forEach((element) => {
      this.total = this.total + element.selling_price;
    });
  }
  render() {
    return (
      <table className="table cart-table brand-2">
        <thead>
          <tr className="thead">
            <th className="description">Face Value(₹)</th>
            <th className="price">Price(₹)</th>
            <th className="pa">Promo Applied</th>
            <th className="quantity">Qty</th>
            <th className="price">Total(₹)</th>
          </tr>
        </thead>
        <tbody>
          {
                        this.props.brandProductdata.map((item) => (
                          <tr>
                            <td className="description"><span>{item.mrp}</span></td>
                            <td className="price">
                              <div><span>{item.selling_price}</span></div>
                            </td>
                            <td className="pa">
                              <div>
                                <p>10% Off <a href="test" title="Edit" className="editIcon"><i className="fas fa-pencil-alt"></i></a></p>
                              </div>
                            </td>
                            <td className="quantity">
                              <div className="define-quantity">
                                <input type="text" name="quantity" value="0" className="qty" />
                                <span className="inc button">+</span><span className="dec button">-</span>
                              </div>
                            </td>
                            <td className="price" id="priceHeader"><span>{item.selling_price}</span></td>
                          </tr>
                        ))
                    }
        </tbody>
        <tfoot>
          <tr className="total-pay">
            <td colSpan="4">Total</td>
            <td className="description"><img src={rupeeIcon} style={{ height: '25px' }} alt="test" />{this.total}</td>
          </tr>
        </tfoot>
      </table >
    );
  }
}

BrandProduct.propTypes = {
  brandProductdata: PropTypes.array,
};

export default BrandProduct;
