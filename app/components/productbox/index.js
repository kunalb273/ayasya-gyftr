import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faHeart from '@fortawesome/fontawesome-free-solid/faHeart';
import ProductThumb from '../../styles/images/1.jpg';
import BrandThumb from '../../styles/images/jj.jpg';

// eslint-disable-next-line
class ProductBox extends Component {
  render() {
    return (
      <div className="product-box1">
        <div className="product-name">
          <h3><span>JACK &amp; JONESh</span></h3>
          <div className="fevrate-box active">
            <FontAwesomeIcon icon={faHeart} onClick={() => this.props.addProductToWishList('25')} />
          </div>
        </div>
        <figure>
          <img src={ProductThumb} alt="" className="w-100" />
          <figcaption>
            <div>
              <div className="box-1">
                <span>SAVE UPTO</span>
                <span className="percents">17%</span>
                <span>OFF</span>
                <div className="share">
                  <ul>
                    <li><a href="test"><i className="fab fa-facebook-square"></i></a></li>
                    <li><a href="test"><i className="fab fa-linkedin"></i></a></li>
                    <li><a href="test"><i className="fab fa-google-plus-square"></i></a></li>
                    <li><a href="test"><i className="fab fa-twitter-square"></i></a></li>
                  </ul>
                </div>
              </div>
              <div>
                <img src={BrandThumb} alt="" />
              </div>
            </div>
          </figcaption>
        </figure>
        <div className="bottom-link d-flex justify-content-between">
          <a href="test">know more</a>
          <a href="test" data-toggle="modal" data-target="#buypopup">ADD TO CART</a>
        </div>
      </div>
    );
  }
}

ProductBox.propTypes = {
  addProductToWishList: PropTypes.func,
};

export default ProductBox;
