import React, { Component } from 'react';
import offerThumb from '../../styles/images/hdfc_image.png';

/* eslint-disable*/
export default class ApplyOffer extends Component {
  render() {
    return (
      <li>
        <input type="radio" name="applyOffer" value="small1" id="one" checked="" />
        <label htmlFor="one"><img src={offerThumb} className="img-fluid" alt="img-fluid" /></label>
      </li>
    );
  }
}
