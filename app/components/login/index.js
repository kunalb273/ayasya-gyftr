import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';


export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
    };
  }

  render() {
    return (
      <section className="row middle-section login-arya">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-lg-5 col-md-6 col-sm-12">
              <h4>Sign in to Continue</h4>
              {this.props.error === null ?
                <p>Enter your e-mail address and password to log in.</p>
                : <p style={{ fontSize: '15px', color: 'red' }}>{this.props.error}</p>
              }

              <div className="custom-form loginForm" >
                <div className="form-group">
                  <label htmlFor="loginEmail1">Email address</label>
                  <input
                    type="email"
                    className="form-control"
                    id="loginEmail1"
                    value={this.state.userName}
                    onChange={(event) => { this.setState({ userName: event.target.value }); }}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="loginPassword">Password</label>
                  <input
                    type="password"
                    className="form-control"
                    id="loginPassword"
                    value={this.state.password}
                    onChange={(event) => { this.setState({ password: event.target.value }); }}
                  />
                  <div className="text-right"><small id="emailHelp" className="form-text text-muted"><a href="test">Forgot password?</a></small></div>
                </div>
                <button className="btn btn-outline-secondary" onClick={() => this.props.loginUser(this.state)}>Login</button>
              </div>
            </div>
            <div className="col-lg-5 col-md-6 col-sm-12">
              <h4>Create an Account</h4>
              <p>Sed eu nisl eu sem auctor dictum sed id nisi. Praesent in arcu acante accumsan venenatis non quis mi. Pellentesque facilisis tellus id odio pellentesque faucibus.</p>
              <p>Sed eu nisl eu sem auctor dictum sed id nisi. Praesent in arcu acante accumsan venenatis non quis mi. Pellentesque facilisis tellus id odio pellentesque faucibus.</p>
              <p><Link to="/auth/registration" className="btn btn-outline-secondary">Create an Account</Link></p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func,
  error: PropTypes.string,
};
