import React, { Component } from 'react';

 /*eslint-disable*/
export default class MoreAboutBrands extends Component {
  render() {
    return (
      <div className="col-sm-12">
        <h3>MORE ABOUT {this.props.brandData.name}</h3>
        <p>{this.props.brandData.long_description}</p>
      </div>
    );
  }
}
