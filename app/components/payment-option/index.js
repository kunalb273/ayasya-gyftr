import React, { Component } from 'react';
import payOptionThumb from '../../styles/images/vw.png';

// eslint-disable-next-line
export default class PaymentOptions extends Component {
  render() {
    return (
      <button className="btn btn-block btn-outline-secondary">
        <span><img src={payOptionThumb} alt="" /></span>
        <span>USE PAYBACK POINTS TO REDEEM</span>
      </button>
    );
  }
}
