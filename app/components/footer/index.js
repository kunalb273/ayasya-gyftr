import React, { Component } from 'react';
import AndroidThumb from '../../styles/images/app_android_v1.png';
import IosThumb from '../../styles/images/app_ios_v1.png';
export default class Footer extends Component {
  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('open-nav');
  }
  render() {
    return (
      <footer className="row footer-arya">
        <div className="container">
          <div className="row">
            <div className="col-md-9">
              <div className="row">
                <div className="col-sm-6 col-md-4">
                  <h4>Categories</h4>
                  <ul className="footer-linke">
                    <li><a href="test">Apparel &amp; Accessories</a></li>
                    <li><a href="test">Food &amp; Beverage</a></li>
                    <li><a href="test">Mobile &amp; Electronics</a></li>
                    <li><a href="test">Health &amp; Wellness</a></li>
                    <li><a href="test">Movie &amp; Magazines</a></li>
                    <li><a href="test">Cabs &amp; Travel</a></li>
                    <li><a href="test">e-Com &amp; Online</a></li>
                    <li><a href="test">Grocery &amp; Home Needs</a></li>
                  </ul>
                </div>
                <div className="col-sm-6 col-md-4">
                  <h4>Quick Links</h4>
                  <ul className="footer-linke">
                    <li><a href="test">Login</a></li>
                    <li><a href="test">Wishlist</a></li>
                    <li><a href="test">My Cart</a></li>
                    <li><a href="test">Checkout</a></li>
                    <li><a href="test">Terms of Use</a></li>
                    <li><a href="test">Privacy Policy</a></li>
                  </ul>
                </div>
                <div className="col-sm-6 col-md-4">
                  <h4>Company</h4>
                  <ul className="footer-linke">
                    <li><a href="test">About Us</a></li>
                    <li><a href="test">News</a></li>
                    <li><a href="test">Press Kit</a></li>
                    <li><a href="test">FAQs</a></li>
                    <li><a href="test">Get in Touch</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <div className="row">
                <div className="col-sm-12">
                  <h4>Download the App </h4>
                  <ul className="list-inline row">
                    <li className="list-inline-item m-0 col-6 pl-0 p-1"><a href="test"><img src={AndroidThumb} alt="test.jpg" /></a></li>
                    <li className="list-inline-item m-0 col-6 pl-0 p-1"><a href="test"><img src={IosThumb} alt="test.jpg" /></a></li>
                  </ul>
                </div>
                <div className="col-sm-12">
                  <h4>100% Secure Payment</h4>
                  <ul className="payment-logos accordion-items">
                    <li className="visa">Visa</li>
                    <li className="mastercard">MasterCard</li>
                    <li className="amex">American Express</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 text-center text-md-right">
              <ul className="list-inline footer-social">
                <li className="list-inline-item"><a href="test"><i className="fab fa-facebook-square"></i></a></li>
                <li className="list-inline-item"><a href="test"><i className="fab fa-linkedin"></i></a></li>
                <li className="list-inline-item"><a href="test"><i className="fab fa-google-plus-square"></i></a></li>
                <li className="list-inline-item"><a href="test"><i className="fab fa-twitter-square"></i></a></li>
                <li className="list-inline-item"><a href="test"><i className="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
