import React, { Component } from 'react';

/*eslint-disable*/
export default class HomeAbout extends Component {
    render() {
        return (
            <div className="col-sm-12">
                <p>{this.props.staticAbout}</p>
            </div>
        );
    }
}
