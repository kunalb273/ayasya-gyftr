import React, { Component } from 'react';
import WishlistProThumb from '../../styles/images/4.jpg';
// eslint-disable-next-line
export default class componentName extends Component {
  render() {
    return (
      <div className="row WishBOXX">
        <div className="table-responsive">
          <table className="table wishlistTable">
            <thead>
              <tr className="thead">
                <th>Brand</th>
                <td></td>
                <th className="delete"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><img src={WishlistProThumb} alt="Brand" /></td>
                <td>
                  <div className="nam-po"><a href="">Levis Strauss &amp; and Company</a></div>
                  <div className="save-po">SAVE UPTO 25% OFF</div>
                  <div className="nam-text">Levi Strauss &amp; Co., an American clothing company, is best known worldwide for its Levis brand of denim jeans. Levis epitomizes classic American style and effortless cool and is one of the most accepted fashion label in India. Use this at 500+ exclusive outlets across India.</div>
                </td>
                <td>
                  <div className="wsBtn"><a href="test" data-toggle="modal" data-target="#buypopup">ADD TO CART</a></div>
                  <div className="wsBtn"><a href="test">REMOVE</a></div>
                </td>
              </tr>
              <tr>
                <td><img src={WishlistProThumb} alt="Brand" /></td>
                <td>
                  <div className="nam-po"><a href="">Levis Strauss &amp; and Company</a></div>
                  <div className="save-po">SAVE UPTO 17% OFF</div>
                  <div className="nam-text">Levi Strauss &amp; Co., an American clothing company, is best known worldwide for its Levis brand of denim jeans. Levis epitomizes classic American style and effortless cool and is one of the most accepted fashion label in India. Use this at 500+ exclusive outlets across India.</div>
                </td>
                <td>
                  <div className="wsBtn"><a href="test" data-toggle="modal" data-target="#buypopup">ADD TO CART</a></div>
                  <div className="wsBtn"><a href="test">REMOVE</a></div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
