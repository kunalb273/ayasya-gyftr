import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// eslint-disable-next-line
export default class AccountMenu extends Component {
  render() {
    return (
      <ul className="nav flex-column">
        <li className="nav-item">
          <Link to="/user/details/myaccount" className="nav-link">My Account</Link>
        </li>
        <li className="nav-item">
          <Link to="/user/details/accountaddbook" className="nav-link">Address Book</Link>
        </li>
        <li className="nav-item">
          <Link to="/user/details/myaccount" className="nav-link">My Orders</Link>
        </li>
        <li className="nav-item">
          <Link to="/user/details/wishlist" className="nav-link">MY WISHLIST</Link>
        </li>
        <li className="nav-item last">
          <Link to="/user/details/logout" className="nav-link">Logout</Link>
        </li>
      </ul>
    );
  }
}
