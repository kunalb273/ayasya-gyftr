import React, { Component } from 'react';
import PropTypes from 'prop-types';

// eslint-disable-next-line
export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      mobile: '',
      password: '',
      cpassword: '',
    };
  }
  // validate() {
  //   if (this.state.firstName !== '' && this.state.lastName !== '' && this.state.mobile.match(/^\d{10}$/) && this.state.password === this.state.cpassword) {
  //     this.props.signUpUser(this.state);
  //   }
  // }
  render() {
    return (
      <section className="row middle-section login-arya">
        <div className="container">
          <div className="row align-items-center justify-content-between">
            <div className="col-lg-5 col-md-6 col-sm-12">
              <h4>Registration</h4>
              <p>Fill out the following form in order to register with gyftr.com</p>
              <div className="custom-form signupForm">
                {this.props.registrationSuccess !== null ? <div><h1 style={{ color: 'green', fontSize: '15px' }}>{this.props.registrationSuccess} Please Login To Continue !</h1></div> : null}
                {this.props.registrationFailed !== null ? <div><h1 style={{ color: 'red', fontSize: '15px' }}>{this.props.registrationFailed}</h1></div> : null}
                <div className="form-group">
                  <label htmlFor="signupName">First Name</label>
                  <input type="text" className="form-control" id="signupName" onChange={(event) => this.setState({ firstName: event.target.value })} />
                </div>
                <div className="form-group">
                  <label htmlFor="signupSurname">Surname</label>
                  <input type="text" className="form-control" id="signupSurname" onChange={(event) => this.setState({ lastName: event.target.value })} />
                </div>
                <div className="form-group">
                  <label htmlFor="signupMobile">Mobile No.</label>
                  <input type="text" className="form-control" id="signupMobile" onChange={(event) => this.setState({ mobile: event.target.value })} />
                </div>
                <div className="form-group">
                  <label htmlFor="signupEmail1">Email address</label>
                  <input type="email" className="form-control" id="signupEmail1" />
                </div>
                <div className="form-group">
                  <label htmlFor="signupPassword" >Password </label>
                  <input type="password" className="form-control" id="signupPassword" onChange={(event) => this.setState({ password: event.target.value })} />
                </div>
                <div className="form-group">
                  <label htmlFor="signupPasswordConfirm">Confirm  Password</label>
                  <input type="password" className="form-control" id="signupPasswordConfirm" onChange={(event) => this.setState({ cpassword: event.target.value })} />
                </div>
                <div className="form-group">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="tc" />
                    <label className="custom-control-label" htmlFor="tc"> By logging on to the site I hereby agree with the <a href="test">Term and Conditions</a> and explicitly allow GyFTR to contact me via e-mail, SMS, Phone and will send you relevant alerts.</label>
                  </div>
                </div>
                <button className="btn btn-outline-secondary" onClick={() => this.props.signUpUser(this.state)}>Sign Up</button>
              </div>
            </div>
            <div className="col-lg-5 col-md-6 col-sm-12 mb-5">
              <div className="mb-3"><button className="loginBtn loginBtn--facebook">Login with Facebook</button></div>
              <div className="mb-3 mt-3"><button className="loginBtn loginBtn--google">Login with Google</button></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

Register.propTypes = {
  signUpUser: PropTypes.func,
  registrationFailed: PropTypes.string,
  registrationSuccess: PropTypes.string,
};
