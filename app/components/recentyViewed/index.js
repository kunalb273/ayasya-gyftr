import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel2';
import '../../styles/css/owl.theme.default.css';
import '../../styles/css/owl.css';
import slide1 from '../../styles/images/fullimage1.jpg';
import slide2 from '../../styles/images/fullimage2.jpg';
import slide3 from '../../styles/images/fullimage3.jpg';

/* eslint-disable */
export default class RecentlyViewed extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      items: [
        <div key={1} className="item"><img src={slide1} alt="The Last of us" /></div>,
        <div key={2} className="item"><img src={slide2} alt="GTA V" /></div>,
        <div key={3} className="item"><img src={slide3} alt="Mirror Edge" /></div>,
      ],

      itemNo: 3,
// margin-left: 25px,
      loop: true,
      nav: false,
      rewind: true,
      autoplay: true,
      responsiveRefreshRate: 500,
    };
  }

  startPlay() {
    this.setState({
      autoplay: true,
    });
  }

  stopPlay() {
    this.setState({
      autoplay: false,
    });
  }

  addItem() {
    const items = this.state.items;
    const newItem = <div key={this.state.items.length + 1} className="item"><img src="/styles/images/fullimage2.jpg" alt="GTA V" /></div>;
    items.push(newItem);
    this.setState({ items });
  }

  newOptions() {
    this.setState({
      nav: true, // Show next and prev buttons
    });
  }
  render() {
    const options = {
      items: this.state.itemNo,
      loop: this.state.loop,
      nav: this.state.nav,
      rewind: this.state.rewind,
      autoplay: this.state.autoplay,
    };

    const events = {
      onDragged(event) { console.log(`onDragged: ${event.type}`); },
      onChanged(event) { console.log(`onChanged: ${event.type}`); },
      onTranslate(event) { console.log(`onTranslate: ${event.type}`); },
    };
    return (
      <div>
        <OwlCarousel
          ref="car"
          options={options}
          events={events}
        >
          {this.state.items}
        </OwlCarousel>

        <button onClick={() => this.refs.car.prev()}>
				prev
			</button>

        <button onClick={() => this.refs.car.next()}>
				next
			</button>

        <button onClick={() => this.refs.car.goTo(0)}>
				goTo
			</button>

        <button onClick={this.startPlay.bind(this)}>
				play
			</button>

        <button onClick={this.stopPlay.bind(this)}>
				stop
			</button>

        <button onClick={this.addItem.bind(this)}>
				Add New
			</button>

        <button onClick={this.newOptions.bind(this)}>
				New Options
			</button>
      </div>
    );
  }
}
