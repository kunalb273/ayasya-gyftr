import React, { Component } from 'react';

// eslint-disable-next-line
export default class MyAccount extends Component {
  render() {
    return (
      <div>
        <div className="col-12">
          <h3>HELLO, MR. ZEYAUL HAQUE!</h3>
          <p>From your My Account dashboard you can see a snapshot of your recent account activity and can update your account information. To view or edit information, please click one of the links below.</p>
        </div>
        <div className="row1">
          <div className="col-6 aressBox">
            <h4>ACCOUNT INFORMATION</h4>
            <div className="box">
              <address>
                <table className="table-address">
                  <tbody>
                    <tr>
                      <th>Name:</th>
                      <td>
                        <div>Mr. zeyaul haque</div>
                        <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                      </td>
                    </tr>
                    <tr>
                      <th>Mobile:</th>
                      <td>
                        <div>9910448494</div>
                        <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                      </td>
                    </tr>
                    <tr>
                      <th>Email:</th>
                      <td>
                        <div>zeyaul.web@gmail.com</div>
                        <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                      </td>
                    </tr>
                    <tr>
                      <th>Password:</th>
                      <td>
                        <div>*******</div>
                        <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </address>
            </div>
          </div>
        </div>
        <div className="row1">
          <div className="col-12 aressBox">
            <h4>ADDRESS BOOK</h4>
            <div className="box box2">
              <address>
                <h5>DEFAULT ADDRESS</h5>
                <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                <p>
                                    Mr. Zeyaul Haque <br />
                                    E: zeyaul@jaywalker.io<br />
                                    T: 9910448494<br />
                </p>
              </address>
            </div>
            <div className="box box2">
              <address>
                <h5>DEFAULT ADDRESS</h5>
                <div><a className="link-Edit" title="Edit" href="account-information.html">Edit</a></div>
                <p>
                                    Mr. Zeyaul Haque <br />
                                    E: zeyaul@jaywalker.io<br />
                                    T: 9910448494<br />
                </p>
              </address>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
