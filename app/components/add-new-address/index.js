import React, { Component } from 'react';

// eslint-disable-next-line
export default class AddNewAddress extends Component {
  render() {
    return (
      <div className="col-12">
        <h3 className="mb-3">CONTACT INFORMATION</h3>
        <form className="custom-form">
          <div className="row">
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="title">TITLE</label>
                <div className="custom-select1">
                  <select id="title" className="form-control">
                    <option value="Ms.">Ms</option>
                    <option value="Mr." selected="selected">Mr</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <input type="text" className="form-control" id="firstName" />
              </div>
            </div>
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="LastName">Last Name</label>
                <input type="text" className="form-control" id="LastName" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="mobile">Mobile Number</label>
                <input type="text" className="form-control" id="mobile" />
              </div>
            </div>
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="EmailAddress">Email Address</label>
                <input type="email" className="form-control" id="EmailAddress" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="one" />
                <label className="custom-control-label" htmlFor="one">Use as my default billing address</label>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="two" />
                <label className="custom-control-label" htmlFor="two">Use as my default shipping address</label>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 mt-4">
              <button type="submit" className="btn btn-outline-secondary mr-5">BACK</button>
              <button type="submit" className="btn btn-outline-secondary">SAVE</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
