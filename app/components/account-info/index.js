import React, { Component } from 'react';
// eslint-disable-next-line
export default class AccountInfo extends Component {
  render() {
    return (
      <div className="col-12">
        <h3 className="mb-3">Account Information</h3>
        <form className="custom-form">
          <div className="row">
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="title">TITLE</label>
                <div className="custom-select1">
                  <select id="title" className="form-control">
                    <option value="Ms.">Ms</option>
                    <option value="Mr." selected="selected">Mr</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <input type="text" className="form-control" id="firstName" value="Zeyaul Haque" />
              </div>
            </div>
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="LastName">Last Name</label>
                <input type="text" className="form-control" id="LastName" value="Haque" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="LastName">Mobile Number</label>
                <input type="text" className="form-control" id="LastName" value="9910448494" />
              </div>
            </div>
            <div className="col-sm-12 col-md-6">
              <div className="form-group">
                <label htmlFor="email">Email Address</label>
                <input type="email" className="form-control" id="email" value="zeyaul@jaywalker.io" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <div className="custom-control custom-checkbox">
                <input type="checkbox" className="custom-control-input" id="ChangaPsaword" onChange="passwordChanged()" />
                <label className="custom-control-label" htmlFor="ChangaPsaword">Change Password</label>
              </div>
            </div>
          </div>
          <div className="password-Hide">
            <h3 className="mb-3">Account Information</h3>
            <div className="row">
              <div className="col-sm-12">
                <div className="form-group">
                  <label htmlFor="password">Current Password</label>
                  <input type="password" className="form-control" id="password" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <div className="form-group">
                  <label htmlFor="NewPassword">New Password</label>
                  <input type="password" className="form-control" id="NewPassword" />
                </div>
              </div>
              <div className="col-sm-12 col-md-6">
                <div className="form-group">
                  <label htmlFor="ConfirmNewPassword">Confirm New Password</label>
                  <input type="password" className="form-control" id="ConfirmNewPassword" />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 mt-4">
              <a href="my-account.html" className="btn btn-outline-secondary mr-5">BACK</a>
              <button type="submit" className="btn btn-outline-secondary">SAVE</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
