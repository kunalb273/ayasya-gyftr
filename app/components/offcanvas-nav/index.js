import React, { Component } from 'react';

// eslint-disable-next-line
export default class OffCanvasNav extends Component {
  render() {
    return (
      <div className="off-canvas-nav">
        <div className="left-nav-toplink">
          <ul className="list-inline">
            <li className="list-inline-item"><a href="test" id="btn2-search" alt="test" ><i className="icon-custom search"></i></a></li>
            <li className="list-inline-item"><a href="wishlist.html" alt="test" ><i className="icon-custom wishlist"></i></a></li>
          </ul>
          <div className="left-nav">
            <ul className="list-unstyled">
              <li><a href="test">New Arrivals</a></li>
              <li className="show-mobile parent"><a href="test" data-related="title_1">Categories</a></li>
              <li><a href="test">Discounts</a></li>
              <li><a href="test">Offers</a></li>
              <li><a href="test">Brands</a></li>
              <li><a href="test">Check My Voucher Status</a></li>
              <li><a href="test">Resend My Voucher</a></li>
              <li><a href="test">My Transactions</a></li>
              <li><a href="test">My Vouchers</a></li>
              <li><a href="test">Customer Care</a></li>
            </ul>
            <div className="subMenu" id="categories">
              <div className="text-right nav-back"><span>Categories</span></div>
              <ul className="list-unstyled">
                <li><a href="category.html">Apparel  Accessories</a></li>
                <li><a href="category.html">Food  Beverage</a></li>
                <li><a href="category.html">Mobile  Electronics</a></li>
                <li><a href="category.html">Health  Wellness</a></li>
                <li><a href="category.html">Movie  Magazines</a></li>
                <li><a href="category.html">Cabs  Travel</a></li>
                <li><a href="category.html">e-Com  Online</a></li>
                <li><a href="category.html">Grocery Home Needs</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div className="leftSearch">
          <div className="form-coopan-box">
            <form>
              <input type="text" readOnly="true" className="form-control" id="" defaultValue="Voucher Details" />
              <button type="button" className="btn btn-primary" href="test" data-toggle="modal" data-target="#voucherDetail">SUBMIT</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
