import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, CustomInput } from 'reactstrap';
import 'react-input-range/lib/css/index.css';
import InputRange from 'react-input-range';

export default class ProductFilter extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdown1Open: false,
      dropdown2Open: false,
      dropdown3Open: false,
      value: 5,
    };
  }
  toggle(count) {
    if (count === '1') {
      this.setState({
        dropdown1Open: !this.state.dropdown1Open,
      });
    } else if (count === '2') {
      this.setState({
        dropdown2Open: !this.state.dropdown2Open,
      });
    } else if (count === '3') {
      this.setState({
        dropdown3Open: !this.state.dropdown3Open,
      });
    }
  }
  render() {
    return (
      <div className="container-fluid filter-arya">
        <div className="container">
          <div className="row justify-content-between">
            <div className="col-lg-9 col-sm-8">
              <div className="row justify-content-center">
                <div className="col-lg-4 col-sm-6">
                  <ButtonDropdown
                    className="btn-block"
                    isOpen={this.state.dropdown1Open}
                    toggle={() => this.toggle('1')}
                  >
                    <DropdownToggle caret>Discount <i></i></DropdownToggle>
                    <DropdownMenu>
                      <div className="range-slider">
                        <InputRange
                          maxValue={100}
                          minValue={0}
                          value={this.state.value}
                          onChange={(value) => this.setState({ value })}
//eslint-disable-next-line 
                          onChangeComplete={(value) => console.log(value)}
                        />
                      </div>
                      <div className="btn-ar">
                        <button className="btn" onClick={this.setState}>Clear</button>
                        <button className="btn">Apply</button>
                      </div>
                    </DropdownMenu>
                  </ButtonDropdown>
                </div>
                <div className="col-lg-4 col-sm-6">
                  <ButtonDropdown
                    className="btn-block"
                    isOpen={this.state.dropdown2Open}
                    toggle={() => this.toggle('2')}
                  >
                    <DropdownToggle caret>Offers <i></i></DropdownToggle>
                    <DropdownMenu>
                      <div className="checkArya">
                        <CustomInput type="checkbox" id="exampleCustomCheckbox1" label="Cards Offer" />
                        <CustomInput type="checkbox" id="exampleCustomCheckbox2" label="Cards Offer 2" />
                        <CustomInput type="checkbox" id="exampleCustomCheckbox3" label="Cards Offer 3" />
                      </div>
                      <div className="btn-ar">
                        <button className="btn">Clear</button>
                        <button className="btn">Apply</button>
                      </div>
                    </DropdownMenu>
                  </ButtonDropdown>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-sm-4">
              <ButtonDropdown
                className="btn-block"
                isOpen={this.state.dropdown3Open}
                toggle={() => this.toggle('3')}
              >
                <DropdownToggle caret>Cards Offer <i></i></DropdownToggle>
                <DropdownMenu>
                  <div className="checkArya">
                    <CustomInput type="checkbox" id="exampleCustomCheckbox4" label="Cards Offer" />
                    <CustomInput type="checkbox" id="exampleCustomCheckbox5" label="Cards Offer 2" />
                    <CustomInput type="checkbox" id="exampleCustomCheckbox6" label="Cards Offer 3" />
                  </div>
                  <div className="btn-ar">
                    <button className="btn">Clear</button>
                    <button className="btn">Apply</button>
                  </div>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
