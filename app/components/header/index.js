import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ProgressiveImage from 'react-progressive-image-loading';
import logo from '../../styles/images/GyfterLogo.png';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: null,
    };
  }


  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('open-nav');
  }


  render() {
    return (
      <div className="headerContainer">
        <header className="row">
          <div className="col-sm-12">
            <div className="row top-gry">
              <div className="container">
                <div className="row">
                  <div className="col-sm-6 text-left">
                    <ul className="list-inline top-links">
                      <li className="list-inline-item">Contact us <span>|</span> +(91) 85 1000 4444</li>
                    </ul>
                  </div>
                  <div className="col-sm-6 text-right">
                    <ul className="list-inline top-links">
                      <li className="list-inline-item">Welcome to GyFTR.com!</li>
                      <li className="list-inline-item"><Link to="/auth/login" >Login/Register</Link></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="row top-white">
              <div className="container">
                <div className="row align-items-center">
                  <div className="col-3 top-left-arye text-left">
                    <div id="hamburger-icon" role="presentation" onClick={this.asideToggle}>
                      <div className="top"></div>
                      <div className="middle"></div>
                      <div className="bottom"></div>
                    </div>
                  </div>
                  <div className="col-6 text-center center-logo">
                    <Link to="/"><img src={logo} className="img-fluid" alt="test.jpg" /></Link>
                  </div>
                  <div className="col-3 top-icon-arye text-right ">
                    <ul className="list-inline">
                      <li className="list-inline-item"><Link to="/user/details/wishlist" id="btn1-search"><i className="icon-custom search"></i></Link></li>
                      <li className="list-inline-item">
                        <Link to="/user/details/wishlist"><i className="icon-custom wishlist"></i><span>2</span></Link>
                      </li>
                      <li className="list-inline-item" id="cart-box">
                        <Link to="/cart"><i className="icon-custom cart"></i><span>{this.props.cartItems !== null ? this.props.cartItems.length : '0'}</span></Link>
                        {
                          this.props.cartItems !== null ?
                            <div className="cart-box">
                              <h3>Your Shopping Cart  ({this.props.cartItems.length} items)</h3>
                              <ul>
                                {this.props.cartItems.map((item) => (<li key={item.cartItemId}>
                                  <div className="row align-items-start">
                                    <div className="col-8">
                                      <h4>{item.product.product_name}</h4>
                                      <h5>Denomination <span>{item.product.selling_price}</span></h5>
                                      <h5>Qty <span>1</span></h5>
                                    </div>
                                    <div className="col-4 text-right">
                                      <span className="priceSem">{item.product.selling_price}</span>
                                    </div>
                                  </div>
                                </li>))}
                              </ul>
                              <div className="row total-arya">
                                <div className="col-6 text-left">
                                  Subtotal
                          </div>
                                <div className="col-6 text-right">
                                  <p><span className="priceSemLarge">6,750</span></p>
                                </div>
                              </div>
                              <div className="button-arya">
                                <a title="Checkout" className="outline-btn" href="payment-options.html">CHECKOUT</a>
                                <Link to="/cart" title="View Shopping Cart" className="outline-btn" href="cart.html">VIEW SHOPPING CART</Link>
                              </div>
                            </div> :
                            <div className="cart-box">
                              <h3>Your Cart is Empty</h3>
                            </div>
                        }
                      </li>
                      <div className="my-overlaw"></div>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <nav className="navbar navbar-expand-md navbar-light bg-light">
          <div className="container">
            <div className="row">
              <div className="collapse navbar-collapse justify-content-md-center" id="mainmenu">
                <ul className="navbar-nav">
                  {
                    //eslint-disable-next-line
                    this.props.data !== null ? this.props.data.categories.map((subCat) => (<li key={subCat.Id} className="nav-item">
                      <Link to={`/category/${subCat.slug}/${subCat.Id}`} className="nav-link">{subCat.name}</Link>
                      <div className="customDropdownMenu">
                        <div className="container">
                          <div className="row">
                            <div className="col-md-9">
                              <div className="row">
                                <div className="col-md-12">
                                  <h4>{subCat.name}</h4>
                                </div>
                                <div className="col-md-4">
                                  <ul className="list-unstyled">
                                    {subCat.brands.map((item) => (<li key={item.id}>
                                      <Link to={`/${item.slug}`}>
                                        {item.name}
                                      </Link>
                                    </li>
                                    ))
                                    }
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div className="col-md-3 menu-images">
                              <ProgressiveImage
                                className="d-block w-100"
                                src={subCat.image_url ? subCat.image_url : '#'}
                                render={(src, style) => <img src={src} alt={src} style={style} />}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    )) :
                    <div></div>
                  }
                </ul>
              </div>
            </div>
          </div>
        </nav>
        <div className="row">
          <div className="form-coopan">
            <div className="container">
              <div className="form-coopan-box">
                <form>
                  <input type="text" readOnly="" className="form-control" id="" value="Voucher Details" />
                  <button type="button" className="btn btn-primary" href="test" data-toggle="modal" data-target="testvoucherDetail">SUBMIT</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}


Header.propTypes = {
  data: PropTypes.object,
  cartItems: PropTypes.array,
};

export default Header;
