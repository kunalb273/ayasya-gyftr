import React, { Component } from 'react';

// eslint-disable-next-line
export default class AccountAddBook extends Component { 
  render() {
    return (
      <div className="col-12">
        <div className="row">
          <div className="col-6 aressBox">
            <h4>DEFAULT ADDRESSES</h4>
            <div className="box">
              <address>
                <h5>DEFAULT ADDRESS 1</h5>
                <p>
                                    Mr. Zeyaul Haque <br />
                                    E: zeyaul@jaywalker.io<br />
                                    T: 9910448494<br />
                </p>
                <div><a className="link-Edit" title="Edit" href="new-address.html">Edit</a></div>
              </address>
            </div>
            <div className="box">
              <address>
                <h5>ALTERNET ADDRESS</h5>
                <p>
                                    Mr. Zeyaul Haque <br />
                                    E: zeyaul@jaywalker.io<br />
                                    T: 9910448494<br />
                </p>
                <div><a className="link-Edit" title="Edit" href="new-address.html">Edit</a></div>
              </address>
            </div>
          </div>
          <div className="col-6 aressBox">
            <h4>ADDITIONAL ADDRESS ENTRIES</h4>
            <div className="box">
              <address>
                <p>You have no additional address entries in your address book.</p>
                <div><a className="link-Edit" title="Add New Address" href="new-address.html">Add New Address</a></div>
              </address>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
