import { fromJS } from 'immutable';
import {
  FETCH_BRAND_DETAILS_SUCCESS,
  FETCH_BRAND_DETAILS_FAILED,
  REMOVE_ERROR_FETCHING_BRAND,
  FETCH_BRAND_PRODUCT_DETAILS_SUCCESS,
  FETCH_BRAND_PRODUCT_DETAILS_FAILED,
} from './constants';

const initialState = fromJS({
  brandDetails: null,
  brandProductDetails: null,
  error: null,
  errorProduct: null,
});

function brandReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BRAND_DETAILS_SUCCESS:
      return state
        .set('brandDetails', action.payload.brand);
    case FETCH_BRAND_DETAILS_FAILED:
      return state
        .set('error', action.payload);
    case FETCH_BRAND_PRODUCT_DETAILS_SUCCESS:
      return state
        .set('brandProductDetails', action.payload);
    case FETCH_BRAND_PRODUCT_DETAILS_FAILED:
      return state
        .set('errorProduct', action.payload);
    case REMOVE_ERROR_FETCHING_BRAND:
      return state
        .set('error', null);
    default:
      return state;
  }
}

export default brandReducer;
