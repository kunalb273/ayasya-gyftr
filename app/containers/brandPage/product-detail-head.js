import React, { Component } from 'react';
import propTypes from 'prop-types';

// eslint-disable-next-line
class ProductDetailsHead extends Component {
  render() {
    const brandData = this.props.brandData;
    return (
      <div className="product-details-head">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-sm-4 col-md-3 col-lg-2 text-center">
              <div><img src={brandData.icon_url} alt="" /></div>
              <div><a className="btn btn-dark btn-block" href="test" role="button" id="btn1-StoreLocater">Store Locater</a></div>
            </div>
            <div className="col-sm-8 col-md-7 col-lg-6">
              <div className="row">
                <div className="col-sm-12">
                  <div className="star-rating">
                    <span className="ratings star-4-5"></span>
                    <span className="countss">(3856)</span>
                  </div>
                </div>
                <div className="col-sm-12">
                  <h3>{brandData.name}</h3>
                </div>
                <div className="col-sm-12">
                  <div className="btn-Social">
                    <input type="checkbox" id="Social-btn" name="Social-btn" />
                    <label htmlFor="Social-btn">
                      <a href="https://www.facebook.com/" className="btn-new btn-circle" target="_blank"><i className="fab fa-facebook-f icon" aria-hidden="true"></i>SMS</a>
                      <a href="https://www.twitter.com/" className="btn-new btn-circle" target="_blank"><i className="fab fa-twitter icon" aria-hidden="true"></i>SMS</a>
                      <a href="https://www.google.com/" className="btn-new btn-circle" target="_blank"><i className="icon" aria-hidden="true">SMS</i>SMS</a>
                      <span className="btn-new btn-circle"><i className="fa fa-times icon"></i></span>
                      <i className="fa fa-share-alt icon" aria-hidden="true"></i>
                    </label>
                  </div>
                </div>
                <div className="col-sm-12">
                  <p>{brandData.short_description}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductDetailsHead.propTypes = {
  brandData: propTypes.object,
};
export default ProductDetailsHead;
