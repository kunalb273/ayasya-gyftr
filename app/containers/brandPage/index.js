import React, { Component } from 'react';
import Wrapper from '../wrapper';
import Brand from './brand';

// eslint-disable-next-line
export default class BrandPage extends Component {
  render() {
    return (
      <Wrapper>
        <Brand />
      </Wrapper>
    );
  }
}
