import { call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_BRAND_DETAILS, FETCH_BRAND_PRODUCT_DETAILS } from './constants';
import {
  fetchBrandProductDetails,
  fetchBrandDetailsSuccess,
  fetchBrandDetailsFailed,
  removeErrorFetchingBrand,
  fetchBrandProductDetailsSuccess,
  fetchBrandProductDetailsFailed,
} from './actions';
import { fetchBrandPageDetails, fetchBrandPageProductDetails } from '../../services';


export function* getBrandPageDetails(payload) {
  const brand = payload.payload.pathname;
  try {
    const resObj = yield call(fetchBrandPageDetails, brand);
    if (resObj.code === 200) {
      yield put(removeErrorFetchingBrand());
      yield put(fetchBrandDetailsSuccess(resObj));
    } else {
      yield put(fetchBrandDetailsFailed(resObj));
    }
  } catch (err) {
    yield put(fetchBrandDetailsFailed(err));
  }
  yield put(fetchBrandProductDetails(brand));
}


export function* getBrandPageProductDetails(payload) {
  try {
    const brand = payload.payload;
    const resObj = yield call(fetchBrandPageProductDetails, brand);
    if (resObj.data.data.code === 200) {
      yield put(removeErrorFetchingBrand());
      yield put(fetchBrandProductDetailsSuccess(resObj.data.data.products));
    } else {
      yield put(fetchBrandProductDetailsFailed(resObj));
    }
  } catch (err) {
    yield put(fetchBrandProductDetailsFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* BrandPage() {
  yield takeEvery(FETCH_BRAND_DETAILS, getBrandPageDetails);
  yield takeEvery(FETCH_BRAND_PRODUCT_DETAILS, getBrandPageProductDetails);
}
