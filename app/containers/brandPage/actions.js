import {
  FETCH_BRAND_DETAILS,
  FETCH_BRAND_DETAILS_SUCCESS,
  FETCH_BRAND_DETAILS_FAILED,
  FETCH_BRAND_PRODUCT_DETAILS,
  FETCH_BRAND_PRODUCT_DETAILS_SUCCESS,
  FETCH_BRAND_PRODUCT_DETAILS_FAILED,
  REMOVE_ERROR_FETCHING_BRAND,
} from './constants';

export function fetchBrandDetails(payload) {
  return {
    type: FETCH_BRAND_DETAILS,
    payload,
  };
}

export function fetchBrandDetailsSuccess(payload) {
  return {
    type: FETCH_BRAND_DETAILS_SUCCESS,
    payload,
  };
}

export function fetchBrandDetailsFailed(payload) {
  return {
    type: FETCH_BRAND_DETAILS_FAILED,
    payload,
  };
}

export function fetchBrandProductDetails(payload) {
  return {
    type: FETCH_BRAND_PRODUCT_DETAILS,
    payload,
  };
}

export function fetchBrandProductDetailsSuccess(payload) {
  return {
    type: FETCH_BRAND_PRODUCT_DETAILS_SUCCESS,
    payload,
  };
}

export function fetchBrandProductDetailsFailed(payload) {
  return {
    type: FETCH_BRAND_PRODUCT_DETAILS_FAILED,
    payload,
  };
}


export function removeErrorFetchingBrand() {
  return {
    type: REMOVE_ERROR_FETCHING_BRAND,
  };
}
