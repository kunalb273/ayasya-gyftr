
import { createSelector } from 'reselect';

const selectBrand = (state) => state.get('brand');
const selectRoute = (state) => state.get('route');

const makeSelectLocation = () => createSelector(
    selectRoute,
    (routeState) => routeState.get('location').toJS()
);

const makeSelectBrandData = () => createSelector(
    selectBrand,
    (state) => state.get('brandDetails')
);

const makeSelectBrandProductData = () => createSelector(
    selectBrand,
    (state) => state.get('brandProductDetails')
);

const makeSelectError = () => createSelector(
    selectBrand,
    (state) => state.get('error')
);

export {
    makeSelectBrandData,
    makeSelectLocation,
    makeSelectError,
    makeSelectBrandProductData,
};
