import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { fetchBrandDetails } from './actions';
import { makeSelectBrandData, makeSelectLocation, makeSelectError, makeSelectBrandProductData } from './selectors';
import reducer from './reducer';
import saga from './saga';


import ProductDetailHead from './product-detail-head';
import ApplyOffer from '../../components/applyoffer';
import PaymentOptions from '../../components/payment-option';
import Adds from '../../components/adds';
import MoreAboutBrands from '../../components/more-about-brands';
import BrandProduct from '../../components/brandProuducts';

// eslint-disable-next-line
class Brand extends Component {

  componentWillMount() {
    const brandName = this.props.brandName;
    this.props.fetchBrandDetails(brandName);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.brandName !== this.props.brandName) {
      this.props.fetchBrandDetails(nextProps.brandName);
    }
    if (nextProps.fetchError !== null) {
      this.props.history.push('/PageNotFound/404');
    }
  }
  render() {
    return (
      <div>
        {this.props.brandData !== null ? <Helmet
          titleTemplate={this.props.brandData.meta_title}
          defaultTitle={this.props.brandData.meta_title}
        >
          <meta name={this.props.brandData.meta_description} content={this.props.brandData.meta_keywords} />
        </Helmet> : null}
        <section className="row">
          <div className="col-sm-12">
            {this.props.brandData !== null ? <ProductDetailHead brandData={this.props.brandData} /> : null}
          </div>
        </section>
        <section className="row detailsArya">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-lg-9 col-sm-12">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="table-responsive">
                      {this.props.brandProductdata !== null ? <BrandProduct brandProductdata={this.props.brandProductdata} /> : null}
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="row">
                      <div className="col-sm-12 col-md-8 col-lg-6">
                        <div className="custom-control custom-checkbox">
                          <input type="checkbox" id="Gyfting" name="GyftingRecipient" className="custom-control-input" />
                          <label className="custom-control-label" htmlFor="Gyfting" >I am Gyfting</label>
                        </div>
                      </div>
                      <div className="col-sm-12 gyftrGifts" id="GyftingRecipientBox1">
                        <form className="custom-form">
                          <div className="row">
                            <div className="col-sm-6">
                              <div className="form-group">
                                <div className="custom-select1">
                                  <label htmlFor="SelectOccasion"></label>
                                  <select name="occasion" className="form-control" id="SelectOccasion">
                                    <option>Select Occasion</option>
                                    <option value="Birthday">Birthday</option>
                                    <option value="Graduation">Graduation</option>
                                    <option value="Thank You">Thank You</option>
                                    <option value="Farewell">Farewell</option>
                                    <option value="Wedding">Wedding</option>
                                    <option value="Anniversary">Anniversary</option>
                                    <option value="Baby Shower">Baby Shower</option>
                                  </select>
                                </div>
                              </div>
                              <div className="form-group">
                                <div className="img-container">
                                  <img id="changeMe" src="images/happyBirthday.png" className="w-100" alt="test" />
                                </div>
                                <div>
                                  <ul className="list-thumb">
                                    <li><img className="preview" src="images/happyBirthday.png" alt="test" /></li>
                                    <li><img className="preview" src="images/happyBirthday.jpg" alt="test" /></li>
                                    <li><img className="preview" src="images/happyBirthday.png" alt="test" /></li>
                                    <li><img className="preview" src="images/happyBirthday.jpg" alt="test" /></li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div className="col-sm-6">
                              <div className="form-group">
                                <label htmlFor="FullName">Full Name</label>
                                <input type="text" className="form-control" id="FullName" />
                              </div>
                              <div className="form-group">
                                <label htmlFor="EmailID">Email ID</label>
                                <input type="email" className="form-control" id="EmailID" />
                              </div>
                              <div className="form-group">
                                <label htmlFor="MobileNo">Mobile No</label>
                                <input type="text" className="form-control" id="MobileNo" />
                              </div>
                              <div className="form-group">
                                <label htmlFor="DeliveryDate">Delivery Date</label>
                                <input type="text" className="form-control hasDatepicker" id="DeliveryDate" />
                              </div>
                              <div className="form-group newHi">
                                <label htmlFor="WriteMessage">Write a Message</label>
                                <textarea className="form-control" id="WriteMessage" rows="2"></textarea>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-12 text-right btnsG">
                    <a href="payment-options.html" className="btn btn-secondary">CHECKOUT</a>
                    <a href="cart.html" className="btn btn-outline-secondary">ADD TO CART</a>
                  </div>
                </div>
                <div className="col-sm-12 CreatOnCombo mt-4">
                  <Adds />
                </div>
              </div>
              <div className="col-md-4 col-lg-3 col-sm-12">
                <div className="row">
                  <div className="col-sm-12 promocode">
                    <h4>Promo &amp; Offers</h4>
                    <form>
                      <input type="text" className="form-control" id="" value="" placeholder="HAVE OFFER CODE" />
                      <button type="submit" className="btn btn-primary">APPLY</button>
                    </form>
                  </div>
                  <div className="col-sm-12 offer-box homePage">
                    <div className="box-forCoopan">
                      <div className="row no-gutters">
                        <div className="col-12">
                          <h4>10% Cash Back <br />with HDFC Debit Cards</h4>
                        </div>
                        <div className="col-5"><img src="images/hdfc-small.png" alt="" /></div>
                        <div className="col-7">
                          <a href="test" className="loginLoyalty">LOGIN TO REDEEM YOUR LOYALTY POINT</a>
                        </div>
                      </div>
                    </div>
                    <div className="btns-groups applyOfferBOx">
                      <ul className="list-unstyled card-list">
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-12 Payment-offer-box">
                    <h4>Payment Options</h4>
                    <div className="btns-groups2">
                      <PaymentOptions />
                      <PaymentOptions />
                      <PaymentOptions />
                    </div>
                  </div>
                  <div className="col-sm-12 promocode">
                    <form>
                      <input type="text" className="form-control" id="" value="" placeholder="HAVE OFFER CODE" />
                      <button type="submit" className="btn btn-primary">APPLY</button>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-sm-12 ii">
                <div className="row">
                  <div className="col-sm-12">
                    {this.props.brandData !== null ? <div dangerouslySetInnerHTML={{ __html: this.props.brandData.how_to_redeem }} /> : null}
                  </div>
                </div>
              </div>
              <div className="col-sm-12 tnc">
                <div className="row">
                  <div className="col-sm-12">
                    {this.props.brandData !== null ? <div dangerouslySetInnerHTML={{ __html: this.props.brandData.important_instruction }} /> : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
        <section className="row pt-4 pb-2 MoreAboutBrands">
          <div className="container">
            <div className="row">
              {this.props.brandData !== null ? <MoreAboutBrands brandData={this.props.brandData} /> : null}
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    fetchBrandDetails: (payload) => {
      dispatch(fetchBrandDetails(payload));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  brandData: makeSelectBrandData(),
  brandProductdata: makeSelectBrandProductData(),
  brandName: makeSelectLocation(),
  fetchError: makeSelectError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'brand', reducer });
const withSaga = injectSaga({ key: 'brand', saga });

Brand.propTypes = {
  fetchBrandDetails: PropTypes.func,
  brandData: PropTypes.object,
  brandName: PropTypes.object,
  fetchError: PropTypes.object,
  history: PropTypes.object,
  brandProductdata: PropTypes.array,
};

export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(Brand));

