import {
  FETCH_HOME_PAGE_SLIDER_IMAGES,
  FETCH_HOME_PAGE_SLIDER_IMAGES_SUCCESS,
  FETCH_HOME_PAGE_SLIDER_IMAGES_FAILED,
  FETCH_ABOUT_CONTENT,
  FETCH_ABOUT_CONTENT_SUCCESS,
  FETCH_ABOUT_CONTENT_FAILED,
  ADD_ITEM_TO_WISHLIST,
  ADD_ITEM_TO_WISHLIST_SUCCESS,
  ADD_ITEM_TO_WISHLIST_FAILED,
} from './constants';

export function fetchHomePageSlider() {
  return {
    type: FETCH_HOME_PAGE_SLIDER_IMAGES,
  };
}

export function fetchHomePageSliderSuccess(payload) {
  return {
    type: FETCH_HOME_PAGE_SLIDER_IMAGES_SUCCESS,
    payload,
  };
}

export function fetchHomePageSliderFailed(payload) {
  return {
    type: FETCH_HOME_PAGE_SLIDER_IMAGES_FAILED,
    payload,
  };
}

export function fetchHomePageStaticContent() {
  return {
    type: FETCH_ABOUT_CONTENT,
  };
}

export function fetchHomePageStaticContentSuccess(payload) {
  return {
    type: FETCH_ABOUT_CONTENT_SUCCESS,
    payload,
  };
}

export function fetchHomePageStaticContentFailed(payload) {
  return {
    type: FETCH_ABOUT_CONTENT_FAILED,
    payload,
  };
}

export function addItemToWishlist(payload) {
  return {
    type: ADD_ITEM_TO_WISHLIST,
    payload,
  };
}

export function addItemToWishlistSuccess(payload) {
  return {
    type: ADD_ITEM_TO_WISHLIST_SUCCESS,
    payload,
  };
}

export function addItemToWishlistFailed(payload) {
  return {
    type: ADD_ITEM_TO_WISHLIST_FAILED,
    payload,
  };
}
