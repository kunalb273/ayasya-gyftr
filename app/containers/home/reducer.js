import { fromJS } from 'immutable';
import {
  FETCH_HOME_PAGE_SLIDER_IMAGES_SUCCESS,
  FETCH_HOME_PAGE_SLIDER_IMAGES_FAILED,
  FETCH_ABOUT_CONTENT_SUCCESS,
  FETCH_ABOUT_CONTENT_FAILED,
  ADD_ITEM_TO_WISHLIST_FAILED,
} from './constants';

const initialState = fromJS({
  homePageSliderImages: null,
  homePageStaticContent: null,
  error: null,
  addwishlistStatus: null,
});

function HomeReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_HOME_PAGE_SLIDER_IMAGES_SUCCESS:
      return state
        .set('homePageSliderImages', action.payload.data.banners);
    case FETCH_HOME_PAGE_SLIDER_IMAGES_FAILED:
      return state
        .set('error', action.payload);
    case FETCH_ABOUT_CONTENT_SUCCESS:
      return state
        .set('homePageStaticContent', action.payload);
    case FETCH_ABOUT_CONTENT_FAILED:
      return state
        .set('error', action.payload);
    case ADD_ITEM_TO_WISHLIST_FAILED:
      return state
        .set('addwishlistStatus', action.payload);
    default:
      return state;
  }
}

export default HomeReducer;
