import React, { Component } from 'react';
import Wrapper from '../wrapper';
import HomePage from './homePage';

// eslint-disable-next-line
export default class Home extends Component {
  render() {
    return (
      <Wrapper>
        <HomePage />
      </Wrapper>
    );
  }
}
