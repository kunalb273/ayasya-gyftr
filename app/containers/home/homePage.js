import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { withRouter } from 'react-router-dom';
import {
  fetchHomePageSlider,
  fetchHomePageStaticContent,
  addItemToWishlist,
} from './actions';
import {
  makeSelectHomePageSliderImages,
  makeSelectHomePageStaticContent,
} from './selectors';
import reducer from './reducer';
import saga from './saga';

import Slider from '../../components/slider/slider';
import ProductFilter from '../../components/productfilter';
import Adds from '../../components/adds';
import HomeAbout from '../../components/home-about';
import ApplyOffer from '../../components/applyoffer';
import PaymentOptions from '../../components/payment-option';
import ProductBox from '../../components/productbox';

// eslint-disable-next-line
class HomePage extends Component {
  constructor(props) {
    super(props);
    this.addProductToWishList = this.addProductToWishList.bind(this);
  }


  componentWillMount() {
    this.props.fetchHomePageSlider();
    this.props.fetchStaticContent();
  }

  addProductToWishList(payload) {
    const itemId = payload;
    if (Cookies.get('Authentication')) {
      this.props.addItemToWishlist(itemId);
    } else {
      //eslint-disable-next-line
      this.props.history.push('/auth/login');
    }
  }

  render() {
    return (
      <main>
        <section className="row">
          {this.props.homeSliderImages !== null ? <Slider sliderImagesData={this.props.homeSliderImages} /> : null}
        </section>
        <section className="row middle-section">
          <ProductFilter />
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-sm-12">
                <div className="row product-gradArya">
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                  <div className="col-sm-12 CreatOnCombo HomeCom">
                    <Adds />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox addProductToWishList={this.addProductToWishList} />
                  </div>
                </div>
              </div>
              {/*  */}
              <div className="col-lg-3 col-sm-12 applyOfferBOx">
                <div className="row">
                  <div className="col-sm-12 offer-box homePage">
                    <div className="box-forCoopan">
                      <div className="row no-gutters">
                        <div className="col-12">
                          <h4>10% Cash Back <br />with HDFC Debit Cards</h4>
                        </div>
                        <div className="col-5"><img src="images/hdfc-small.png" alt="" /></div>
                        <div className="col-7">
                          <a href="test" className="loginLoyalty">LOGIN TO REDEEM YOUR LOYALTY POINT</a>
                        </div>
                      </div>
                    </div>
                    <div className="btns-groups applyOfferBOx">
                      <ul className="list-unstyled card-list">
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-12 Payment-offer-box">
                    <h4>Payment Options</h4>
                    <div className="btns-groups2">
                      <PaymentOptions />
                      <PaymentOptions />
                      <PaymentOptions />

                    </div>
                  </div>
                  <div className="col-sm-12 promocode">
                    <form>
                      <input type="text" className="form-control" id="" value="" placeholder="HAVE OFFER CODE" />
                      <button type="submit" className="btn btn-primary">APPLY</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
              <div className="col-sm-12">
                {/* <RecentlyViewed /> */}
              </div>
            </div>
          </div>
        </section>
        <section className="row pt-4 pb-2 MoreAboutBrands">
          <div className="container">
            <div className="row">
              {this.props.staticAbout !== null ? <HomeAbout staticAbout={this.props.staticAbout} /> : null}
            </div>
          </div>
        </section>
      </main>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    fetchHomePageSlider: () => {
      dispatch(fetchHomePageSlider());
    },
    fetchStaticContent: () => {
      dispatch(fetchHomePageStaticContent());
    },
    addItemToWishlist: (payload) => {
      dispatch(addItemToWishlist(payload));
    },
  };
}


const mapStateToProps = createStructuredSelector({
  homeSliderImages: makeSelectHomePageSliderImages(),
  staticAbout: makeSelectHomePageStaticContent(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

HomePage.propTypes = {
  fetchHomePageSlider: PropTypes.func,
  fetchStaticContent: PropTypes.func,
  homeSliderImages: PropTypes.array,
  staticAbout: PropTypes.array,
  addItemToWishlist: PropTypes.func,
};

export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage));
