
import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectHomePageSliderImages = () => createSelector(
    selectHome,
    (state) => state.get('homePageSliderImages')
);

const makeSelectHomePageStaticContent = () => createSelector(
    selectHome,
    (state) => state.get('homePageStaticContent')
);


export {
    makeSelectHomePageSliderImages,
    makeSelectHomePageStaticContent,
};
