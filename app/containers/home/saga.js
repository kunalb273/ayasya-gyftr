import { call, put, takeEvery } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import { FETCH_HOME_PAGE_SLIDER_IMAGES, FETCH_ABOUT_CONTENT, ADD_ITEM_TO_WISHLIST } from './constants';
import {
  fetchHomePageSliderSuccess,
  fetchHomePageSliderFailed,
  fetchHomePageStaticContentSuccess,
  fetchHomePageStaticContentFailed,
  addItemToWishlistSuccess,
  addItemToWishlistFailed,
} from './actions';
import {
  fetchHomePageSliderImages,
  fetchHomePageStaticContent,
  addToWishlist,
} from '../../services';


export function* gethomePageSliderImages() {
  try {
    const resObj = yield call(fetchHomePageSliderImages);
    if (resObj.status === 200) {
      yield put(fetchHomePageSliderSuccess(resObj.data));
    } else {
      yield put(fetchHomePageSliderFailed(resObj.data));
    }
  } catch (err) {
    yield put(fetchHomePageSliderFailed(err));
  }
}

export function* getHomePageStaticContent() {
  try {
    const resObj = yield call(fetchHomePageStaticContent);
    if (resObj.status === 200) {
      yield put(fetchHomePageStaticContentSuccess(resObj.data.data.content));
    } else {
      yield put(fetchHomePageStaticContentFailed(resObj.data));
    }
  } catch (err) {
    yield put(fetchHomePageStaticContentFailed(err));
  }
}

export function* addItemToWishlist(payload) {
  const itemId = payload.payload;
  const token = yield call(Cookies.get, 'Authentication');
  const data = {
    itemId,
    token,
  };
  try {
    const resObj = yield call(addToWishlist, data);
    if (resObj.data.data.code === 200) {
      yield put(addItemToWishlistSuccess(resObj.data.data.message));
    } else {
      yield put(addItemToWishlistFailed(resObj.data.data.message));
    }
  } catch (err) {
    yield put(addItemToWishlistFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* HomePage() {
  yield takeEvery(FETCH_HOME_PAGE_SLIDER_IMAGES, gethomePageSliderImages);
  yield takeEvery(FETCH_ABOUT_CONTENT, getHomePageStaticContent);
  yield takeEvery(ADD_ITEM_TO_WISHLIST, addItemToWishlist);
}
