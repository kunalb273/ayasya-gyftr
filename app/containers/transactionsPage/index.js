import React, { Component } from 'react';
import Wrapper from '../wrapper';
import Transactions from './transactions';

// eslint-disable-next-line
export default class TransactionPage extends Component {
  render() {
    return (
      <Wrapper>
        <Transactions />
      </Wrapper>
    );
  }
}
