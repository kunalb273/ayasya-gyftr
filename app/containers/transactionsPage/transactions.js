import React, { Component } from 'react';
import CategoryBanner from '../../components/category-banner';

// eslint-disable-next-line
export default class Transactions extends Component {
  render() {
    return (
      <div>
        <section className="row">
          <div className="col-sm-12">
            <CategoryBanner />
          </div>
        </section>
        <section className="row middle-section">
          <div className="container">
            <div className="row text-center">
              <div className="col-sm-12 mt-3">
                <div className="table-responsive">
                  <table className="table tranjection_t br-t-0">
                    <tbody><tr>
                      <th>Order No</th>
                      <th>Date</th>
                      <th>Points Burnt</th>
                      <th>Cash</th>
                      <th>Points Warned</th>
                      <th>Status</th>
                    </tr>
                      <tr>
                        <td>123456-098765</td>
                        <td>01 May 2018</td>
                        <td>200</td>
                        <td>0.00</td>
                        <td>0</td>
                        <td>Completed</td>
                      </tr>
                      <tr>
                        <td>123456-098765</td>
                        <td>01 May 2018</td>
                        <td>200</td>
                        <td>0.00</td>
                        <td>0</td>
                        <td>Failed</td>
                      </tr>
                      <tr>
                        <td>123456-098765</td>
                        <td>01 May 2018</td>
                        <td>200</td>
                        <td>0.00</td>
                        <td>0</td>
                        <td>Completed</td>
                      </tr>
                      <tr>
                        <td>123456-098765</td>
                        <td>01 May 2018</td>
                        <td>200</td>
                        <td>0.00</td>
                        <td>0</td>
                        <td>Completed</td>
                      </tr>
                    </tbody></table>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
