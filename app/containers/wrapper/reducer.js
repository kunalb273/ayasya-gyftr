import { fromJS } from 'immutable';
import {
    FETCH_TOPNAV_PRODUCT_LIST_SUCCESS,
    FETCH_TOPNAV_PRODUCT_LIST_FAILED,
} from './constants';

const initialState = fromJS({
  TopNavProductList: null,
  error: null,
});

function WrapperReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TOPNAV_PRODUCT_LIST_SUCCESS:
      return state
                .set('TopNavProductList', action.payload.data);
    case FETCH_TOPNAV_PRODUCT_LIST_FAILED:
      return state
                .set('error', action.payload);
    default:
      return state;
  }
}

export default WrapperReducer;
