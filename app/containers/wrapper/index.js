import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import { fetchTopNavProductList } from './actions';
import {
  makeSelectTopNavList,
  makeSelectUserCart,
} from './selectors';
import reducer from './reducer';
import saga from './saga';

import '../../styles/css/styles.css';
import '../../styles/css/custom.css';
// Import Global Components
import Header from '../../components/header';
import Footer from '../../components/footer';
import OffCanvasNav from '../../components/offcanvas-nav';
// Import Global Components

/* eslint-disable */
class Wrapper extends Component {

  componentWillMount() {

    this.props.fetchTopNavListItems();
  }

  render() {
    const topNavItemsList = this.props.topNavItemsList;
    return (
      <div>
        <OffCanvasNav />
        <div className="off-canvas-main">
          <div className="container-fluid">
            <Header data={topNavItemsList} cartItems={this.props.cartItems} />
            {this.props.children}
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    fetchTopNavListItems: () => {
      dispatch(fetchTopNavProductList());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  topNavItemsList: makeSelectTopNavList(),
  cartItems: makeSelectUserCart(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'Wrapper', reducer });
const withSaga = injectSaga({ key: 'Wrapper', saga });

Wrapper.PropTypes = {
  fetchTopNavProductList: PropTypes.func,
  topNavItemsList: PropTypes.object,
}

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Wrapper);
