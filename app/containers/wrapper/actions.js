import {
    FETCH_TOPNAV_PRODUCT_LIST,
    FETCH_TOPNAV_PRODUCT_LIST_SUCCESS,
    FETCH_TOPNAV_PRODUCT_LIST_FAILED,
} from './constants';

export function fetchTopNavProductList() {
  return {
    type: FETCH_TOPNAV_PRODUCT_LIST,
  };
}

export function fetchTopNavProductListSuccess(payload) {
  return {
    type: FETCH_TOPNAV_PRODUCT_LIST_SUCCESS,
    payload,
  };
}

export function fetchTopNavProductListFailed(payload) {
  return {
    type: FETCH_TOPNAV_PRODUCT_LIST_FAILED,
    payload,
  };
}
