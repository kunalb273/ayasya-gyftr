
import { createSelector } from 'reselect';

const selectWrapper = (state) => state.get('Wrapper');
const selectUserCart = (state) => state.get('userCart');


const makeSelectUserCart = () => createSelector(
    selectUserCart,
    (state) => state.get('cart')
);


const makeSelectTopNavList = () => createSelector(
    selectWrapper,
    (state) => state.get('TopNavProductList')
);


export {
    makeSelectTopNavList,
    makeSelectUserCart,
};
