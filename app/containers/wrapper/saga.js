import { call, put, takeLatest } from 'redux-saga/effects';
import { FETCH_TOPNAV_PRODUCT_LIST } from './constants';
import { fetchTopNavProductListSuccess, fetchTopNavProductListFailed } from './actions';
import { fetchTopNavList } from '../../services';


export function* getTopNavList() {
  try {
    const resObj = yield call(fetchTopNavList);
    if (resObj.status === 200) {
      yield put(fetchTopNavProductListSuccess(resObj.data));
    } else {
      yield put(fetchTopNavProductListFailed(resObj));
    }
  } catch (err) {
    yield put(fetchTopNavProductListFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* WrapperComponents() {
  yield takeLatest(FETCH_TOPNAV_PRODUCT_LIST, getTopNavList);
}
