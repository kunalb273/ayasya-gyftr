/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'gyftr/App/en';
export const LOAD_REPOS_SUCCESS = 'gyftr/App/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS = 'gyftr/App/LOAD_REPOS';
export const LOAD_REPOS_ERROR = 'gyftr/App/LOAD_REPOS_ERROR';
