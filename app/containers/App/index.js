/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, withRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// import Cookies from 'js-cookie';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
// import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import reducer from './reducer';
/* eslint-disable */
import PrivateRoute from '../../routes/PrivateRoute';
import PublicRoute from '../../routes/PublicRoute';
import Home from '../home';
import CategoryPage from '../productCategory';
import CartPage from '../cart';
import BrandPage from '../brandPage';
import Authentication from '../auth'
import UserDetails from '../userAccountPage';
import TransactionPage from '../transactionsPage';
import ThankyouPage from '../thankyouPage';
import NotFoundPage from '../NotFoundPage/Loadable';

class App extends React.Component {
  render() {
    return (
      <div>
        <Helmet
          titleTemplate="Gyftr"
          defaultTitle="Gyftr"
        >
          <meta name="description" content="Gyftr" />
        </Helmet>
        <Switch>
          <PublicRoute exact path="/" component={Home} />
          <PublicRoute exact path="/category/:name/:id" component={CategoryPage} />
          <PrivateRoute exact path="/cart" component={CartPage} />
          <PublicRoute exact path={"/:brandName"} component={BrandPage} />
          <PublicRoute exact path="/auth/:id" component={Authentication} />
          <PrivateRoute exact path="/user/details/:detailType" component={UserDetails} />
          <PrivateRoute exact path="/user/transaction" component={TransactionPage} />
          <PrivateRoute exact path="/thankyou" component={ThankyouPage} />
          <PublicRoute exact path="/PageNotFound/404" component={NotFoundPage} />
          <PublicRoute component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}

const withReducer = injectReducer({ key: 'global', reducer });

export default withRouter(compose(
  withReducer,
)(App));
