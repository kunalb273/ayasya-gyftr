import React, { Component } from 'react';
import Wrapper from '../wrapper';
import Thankyou from './thankyou';

// eslint-disable-next-line
export default class ThankyouPage extends Component {
  render() {
    return (
      <Wrapper>
        <Thankyou />
      </Wrapper>
    );
  }
}
