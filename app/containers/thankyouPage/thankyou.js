import React, { Component } from 'react';
import CategoryBanner from '../../components/category-banner';
import ThanyouIcon from '../../styles/images/thank-page-image.png';
import ThankyouBannerThumbs from '../../styles/images/banner_thanks_1.jpg';

// eslint-disable-next-line
export default class Thankyou extends Component {
  render() {
    return (
      <div>
        <section className="row">
          <div className="col-sm-12">
            <CategoryBanner />
          </div>
        </section>
        <section className="row middle-section">
          <div className="container">
            <div className="row text-center">
              <div className="col-sm-12">
                <div className="mt-3"><img src={ThanyouIcon} alt="" /></div>
                <div className="mb-3 CongratulationsMsg">
                  <h5>Congratulations!</h5>
                  <p className="m-0">Your Order No(012345-98754) has been processed successfully. you would receve your instant voucher next 10 minutes!</p>
                  <p>For assistance, please call Tollfree number - 7827003910</p>
                </div>
              </div>
              <div className="col-sm-12 mb-4">
                <div className="row">
                  <div className="col-4">
                    <img src={ThankyouBannerThumbs} className="br-1" alt="" />
                  </div>
                  <div className="col-4">
                    <img src={ThankyouBannerThumbs} className="br-1" alt="" />
                  </div>
                  <div className="col-4">
                    <img src={ThankyouBannerThumbs} className="br-1" alt="" />
                  </div>
                </div>
              </div>
              <div className="col-sm-12">
                <table className="table tranjection_t">
                  <tbody>
                    <tr>
                      <th>Order No</th>
                      <th>Date</th>
                      <th>Points Burnt</th>
                      <th>Cash</th>
                      <th>Points Warned</th>
                      <th>Status</th>
                    </tr>
                    <tr>
                      <td>123456-098765</td>
                      <td>01 May 2018</td>
                      <td>200</td>
                      <td>0.00</td>
                      <td>0</td>
                      <td>Completed</td>
                    </tr>
                    <tr>
                      <th>Delivery Email</th>
                      <th>Delivery Mobile</th>
                      <th>Voucher</th>
                      <th>Face Value</th>
                      <th>Quantity</th>
                      <th>Voucher Status</th>
                    </tr>
                    <tr>
                      <td>zeyaul@jaywalker.io</td>
                      <td>09910448494</td>
                      <td>Baskin Robbins</td>
                      <td>50.00</td>
                      <td>1</td>
                      <td>Processed</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
