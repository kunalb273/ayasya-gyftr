import { fromJS } from 'immutable';

import {
  SELECT_USER_DETAILS_TYPE,
} from './constants';

const initialState = fromJS({
  userDetailsPage: null,
  cart: null,
  cartStatus: null,
  addToCartStatus: null,
  deleteFromCartStatus: null,

});

function userAccountDetailReducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_USER_DETAILS_TYPE:
      return state
        .set('userDetailsPage', action.payload.data.data.message);
    default:
      return state;
  }
}

export default userAccountDetailReducer;
