export const SELECT_USER_DETAILS_TYPE = 'gyftr/userAccountPage/SELECT_USER_DETAILS_TYPE';

export const LOGOUT_USER = 'gyftr/userAccountPage/LOGOUT_USER';
export const LOGOUT_USER_SUCCESS = 'gyftr/userAccountPage/LOGOUT_USER_SUCCESS';
export const LOGOUT_USER_FAILED = 'gyftr/userAccountPage/LOGOUT_USER_FAILED';

export const FETCH_WISHLIST_ITEMS = 'gyftr/userAccountPage/FETCH_WISHLIST_ITEMS';
export const FETCH_WISHLIST_ITEMS_SUCCESS = 'gyftr/userAccountPage/FETCH_WISHLIST_ITEMS_SUCCESS';
export const FETCH_WISHLIST_ITEMS_FAILED = 'gyftr/userAccountPage/FETCH_WISHLIST_ITEMS_FAILED';
