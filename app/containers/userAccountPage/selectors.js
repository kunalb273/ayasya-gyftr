import { createSelector } from 'reselect';

const selectUserAccount = (state) => state.get('userAccount');
const selectRoute = (state) => state.get('route');


const makeSelectUserAccountDetailsPage = () => createSelector(
    selectUserAccount,
    (state) => state.get('userDetailsPage')
);

const makeSelectUserAccountPathName = () => createSelector(
    selectRoute,
    (state) => state.getIn(['location', 'pathname'])
);
export {
    makeSelectUserAccountDetailsPage,
    makeSelectUserAccountPathName,
};
