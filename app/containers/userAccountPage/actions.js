import {
  SELECT_USER_DETAILS_TYPE,
  LOGOUT_USER,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILED,
  FETCH_WISHLIST_ITEMS,
  FETCH_WISHLIST_ITEMS_SUCCESS,
  FETCH_WISHLIST_ITEMS_FAILED,
} from './constants';

export function selectUserDetailsType(payload) {
  return {
    type: SELECT_USER_DETAILS_TYPE,
    payload,
  };
}

export function logoutUser(payload) {
  return {
    type: LOGOUT_USER,
    payload,
  };
}

export function logoutUserSuccess(payload) {
  return {
    type: LOGOUT_USER_SUCCESS,
    payload,
  };
}

export function logoutUserFailed(payload) {
  return {
    type: LOGOUT_USER_FAILED,
    payload,
  };
}

export function fetchWishlistItems() {
  return {
    type: FETCH_WISHLIST_ITEMS,
  };
}

export function fetchWishlistItemsSuccess(payload) {
  return {
    type: FETCH_WISHLIST_ITEMS_SUCCESS,
    payload,
  };
}

export function fetchWishlistItemsFailed(payload) {
  return {
    type: FETCH_WISHLIST_ITEMS_FAILED,
    payload,
  };
}
