import React, { Component } from 'react';
import Wrapper from '../wrapper';
import UserDetails from './userDetails';

// eslint-disable-next-line
export default class CategoryPage extends Component {
  render() {
    return (
      <Wrapper>
        <UserDetails />
      </Wrapper>
    );
  }
}
