import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import {
  selectUserDetailsType,
  logoutUser,
  fetchWishlistItems,
} from './actions';
import {
  makeSelectUserAccountDetailsPage,
  makeSelectUserAccountPathName,
} from './selectors';
import reducer from './reducer';
import saga from './saga';

import CategoryBanner from '../../components/category-banner';
import AccountMenu from '../../components/account-menu';
import AccountInfo from '../../components/account-info';
import AccountAddBook from '../../components/account-add-book';
import AddNewAddress from '../../components/add-new-address';
import MyAccount from '../../components/my-account';
import Wishlist from '../../components/wishlist';

class UserDetails extends Component {


  componentWillMount() {
    this.props.fetchWishlistItems();
  }

  componentToRender;

  logOutUser() {
    this.props.onlogoutUser();
    //eslint-disable-next-line
    this.props.location.push("/");
  }

  render() {
    if (this.props.pathName === '/user/details/myaccount') {
      this.componentToRender = <MyAccount />;
    } else if (this.props.pathName === '/user/details/editaccount') {
      this.componentToRender = <AccountInfo />;
    } else if (this.props.pathName === '/user/details/accountaddbook') {
      this.componentToRender = <AccountAddBook />;
    } else if (this.props.pathName === '/user/details/addnewaddress') {
      this.componentToRender = <AddNewAddress />;
    } else if (this.props.pathName === '/user/details/wishlist') {
      this.componentToRender = <Wishlist />;
    } else if (this.props.pathName === '/user/details/logout') {
      this.logOutUser();
    }
    return (
      <div>
        <section className="row">
          <div className="col-sm-12">
            <CategoryBanner />
          </div>
        </section>
        <section className="row middle-section myAccount-arya">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xl-2 col-lg-3 col-md-3 col-sm-4 myAccount-left">
                <h4>Account Menu</h4>
                <AccountMenu />
              </div>
              <div className="col-xl-10 col-lg-9 col-md-9 col-sm-8 myAccount-right addBok">
                {this.componentToRender}
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}


export function mapDispatchToProps(dispatch) {
  return {
    onPageChange: (payload) => {
      dispatch(selectUserDetailsType(payload));
    },
    onlogoutUser: () => {
      dispatch(logoutUser());
    },
    fetchWishlistItems: () => {
      dispatch(fetchWishlistItems());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  userAccountPage: makeSelectUserAccountDetailsPage(),
  pathName: makeSelectUserAccountPathName(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'userAccount', saga });
const withReducer = injectReducer({ key: 'userAccount', reducer });


export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(UserDetails));

UserDetails.propTypes = {
  pathName: PropTypes.string,
  onlogoutUser: PropTypes.func,
  fetchWishlistItems: PropTypes.func,
};
