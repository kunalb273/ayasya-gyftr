import { call, put, takeEvery } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import {
  LOGOUT_USER,
  FETCH_WISHLIST_ITEMS,
} from './constants';
import {
  logoutUserSuccess,
  logoutUserFailed,
} from './actions';
import {
  logoutUser,
} from '../../services';


export function* logOutCurrentUser() {
  const token = yield call(Cookies.get, 'Authentication');
  try {
    const resObj = yield call(logoutUser, token);
    if (resObj.data.data.code === 200) {
      Cookies.remove('Authentication');
      yield put(logoutUserSuccess());
    } else {
      yield put(logoutUserFailed());
      Cookies.remove('Authentication');
    }
  } catch (err) {
    yield put(logoutUserFailed(err));
  }
}

export function* getWishlistItems() {
  // const token = yield call(Cookies.get, 'Authentication');
  // try {
  // const resObj = yield call(viewUserCart, token);
  //   if (resObj.data.data.code === 200) {
  //     Cookies.remove('Authentication');
  //     yield put(logoutUserSuccess());
  //   } else {
  //     yield put(logoutUserFailed());
  //     Cookies.remove('Authentication');
  //   }
  // } catch (err) {
  //   yield put(logoutUserFailed(err));
  // }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* userAccount() {
  yield takeEvery(LOGOUT_USER, logOutCurrentUser);
  yield takeEvery(FETCH_WISHLIST_ITEMS, getWishlistItems);
}
