import { createSelector } from 'reselect';

const selectRoute = (state) => state.get('route');
const selectAuth = (state) => state.get('auth');

const makeSelectAuthPageLocation = () => createSelector(
    selectRoute,
    (state) => state.getIn(['location', 'pathname'])
);

const makeSelectCurrentUser = () => createSelector(
    selectAuth,
    (state) => state.get('currentUser')
);

const makeSelectLoginError = () => createSelector(
    selectAuth,
    (state) => state.get('loginError')
);

const makeSelectRegistrationSuccess = () => createSelector(
    selectAuth,
    (state) => state.get('registrationSuccess')
);
const makeSelectRegistrationFailed = () => createSelector(
    selectAuth,
    (state) => state.get('registerError')
);

export {
    makeSelectAuthPageLocation,
    makeSelectCurrentUser,
    makeSelectRegistrationSuccess,
    makeSelectRegistrationFailed,
    makeSelectLoginError,
};
