import { fromJS } from 'immutable';
import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED, REGISTER_USER_FAILED, REGISTER_USER_SUCCESS,
} from './constants';

const initialState = fromJS({
  currentUser: null,
  loginError: null,
  registerError: null,
  registrationSuccess: null,
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return state
        .set('currentUser', action.payload.data.data);
    case LOGIN_USER_FAILED:
      return state
        .set('loginError', action.payload);
    case REGISTER_USER_FAILED:
      return state
        .set('registerError', action.payload);
    case REGISTER_USER_SUCCESS:
      return state
        .set('registrationSuccess', action.payload);
    default:
      return state;
  }
}

export default authReducer;
