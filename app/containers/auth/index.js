import React, { Component } from 'react';
import Wrapper from '../wrapper';
import Authentication from './authentication';

// eslint-disable-next-line
export default class AuthenticationPage extends Component {
  render() {
    return (
      <Wrapper>
        <Authentication />
      </Wrapper>
    );
  }
}
