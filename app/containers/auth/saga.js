import { call, put, takeEvery } from 'redux-saga/effects';
import { LOGIN_USER, REGISTER_USER } from './constants';
import { loginUserSuccess, loginUserFailed, registerUserSuccess, registerUserFailed } from './actions';
import { logInUser, signupUser } from '../../services';


export function* loginUser(credentials) {
  try {
    const resObj = yield call(logInUser, credentials.payload);
    if (resObj.data.data.code === 200) {
      yield put(loginUserSuccess(resObj));
      yield put(loginUserFailed(null));
    } else {
      yield put(loginUserFailed(resObj.data.data.message));
    }
  } catch (err) {
    yield put(loginUserFailed(err));
  }
}

export function* registerUser(details) {
  try {
    const resObj = yield call(signupUser, details.payload);
    if (resObj.data.data.code === 200) {
      yield put(registerUserFailed(null));
      yield put(registerUserSuccess(resObj.data.data.message));
    } else {
      yield put(registerUserFailed(resObj.data.data.message));
    }
  } catch (err) {
    yield put(registerUserFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* Authentication() {
  yield takeEvery(LOGIN_USER, loginUser);
  yield takeEvery(REGISTER_USER, registerUser);
}
