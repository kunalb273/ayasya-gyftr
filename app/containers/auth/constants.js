export const REGISTER_USER = 'gyftr/auth/REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'gyftr/auth/REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILED = 'gyftr/auth/REGISTER_USER_FAILED';

export const LOGIN_USER = 'gyftr/auth/LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'gyftr/auth/LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILED = 'gyftr/auth/LOGIN_USER_FAILED';
