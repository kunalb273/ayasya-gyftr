import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILED,
    REGISTER_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILED,
} from './constants';

export function loginUser(payload) {
  return {
    type: LOGIN_USER,
    payload,
  };
}

export function loginUserSuccess(payload) {
  return {
    type: LOGIN_USER_SUCCESS,
    payload,
  };
}

export function loginUserFailed(payload) {
  return {
    type: LOGIN_USER_FAILED,
    payload,
  };
}

export function registerUser(payload) {
  return {
    type: REGISTER_USER,
    payload,
  };
}

export function registerUserSuccess(payload) {
  return {
    type: REGISTER_USER_SUCCESS,
    payload,
  };
}

export function registerUserFailed(payload) {
  return {
    type: REGISTER_USER_FAILED,
    payload,
  };
}
