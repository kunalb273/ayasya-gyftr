import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Cookies from 'js-cookie';
import { createStructuredSelector } from 'reselect';
import { loginUser, registerUser } from './actions';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectAuthPageLocation,
  makeSelectCurrentUser,
  makeSelectRegistrationSuccess,
  makeSelectRegistrationFailed,
  makeSelectLoginError,
} from './selectors';
import Login from '../../components/login';
import Register from '../../components/register';
import CategoryBanner from '../../components/category-banner';


class Authentication extends Component {
  constructor(props) {
    super(props);
    this.loginUser = this.loginUser.bind(this);
    this.signUpUser = this.signUpUser.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentUser !== null) {
      if (nextProps.currentUser.token) {
        Cookies.set('Authentication', nextProps.currentUser.token, { expires: 7 });
        // eslint-disable-next-line
        this.props.history.push("/");
      }
    }
  }

  loginUser(credentials) {
    this.props.loginUser(credentials);
  }

  signUpUser(details) {
    this.props.registerUser(details);
  }

  componentToRender;

  render() {
    if (this.props.pathName === '/auth/login') {
      this.componentToRender = <Login loginUser={this.loginUser} error={this.props.loginError} />;
    } else if (this.props.pathName === '/auth/registration') {
      this.componentToRender =
        (<Register
          signUpUser={this.signUpUser}
          registrationSuccess={this.props.registrationSuccess}
          registrationFailed={this.props.registrationFailed}
        />);
    }
    return (
      <div>
        <section className="row">
          <div className="col-sm-12">
            <CategoryBanner />
          </div>
        </section>
        {this.componentToRender}
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    loginUser: (payload) => {
      dispatch(loginUser(payload));
    },
    registerUser: (payload) => {
      dispatch(registerUser(payload));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  pathName: makeSelectAuthPageLocation(),
  currentUser: makeSelectCurrentUser(),
  registrationSuccess: makeSelectRegistrationSuccess(),
  registrationFailed: makeSelectRegistrationFailed(),
  loginError: makeSelectLoginError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'auth', reducer });
const withSaga = injectSaga({ key: 'auth', saga });

export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(Authentication));

Authentication.propTypes = {
  pathName: PropTypes.string,
  currentUser: PropTypes.object,
  loginUser: PropTypes.func,
  registerUser: PropTypes.func,
  registrationFailed: PropTypes.string,
  registrationSuccess: PropTypes.string,
  loginError: PropTypes.string,
};

