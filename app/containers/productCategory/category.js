import React, { Component } from 'react';
import { withRouter } from 'react-router';
import CategoryBanner from '../../components/category-banner';
import ProductFilter from '../../components/productfilter';
import ApplyOffer from '../../components/applyoffer';
import PaymentOptions from '../../components/payment-option';
import ProductBox from '../../components/productbox';

// eslint-disable-next-line
class ProductCategory extends Component {
  render() {
    return (
      <div>
        <section className="row">
          <div className="col-sm-12">
            <CategoryBanner />
          </div>
        </section>
        <section className="row middle-section">
          <ProductFilter />
          <div className="container">
            <div className="row">
              <div className="col-lg-9 col-sm-12">
                <div className="row product-gradArya">
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                  <div className="col-lg-4 col-md-4 col-sm-6">
                    <ProductBox />
                  </div>
                </div>
              </div>
              {/*  */}
              <div className="col-lg-3 col-sm-12 applyOfferBOx">
                <div className="row">
                  <div className="col-sm-12 offer-box homePage">
                    <div className="btns-groups applyOfferBOx">
                      <ul className="list-unstyled card-list">
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                        <ApplyOffer />
                      </ul>
                    </div>
                  </div>
                  <div className="col-sm-12 Payment-offer-box">
                    <h4>Payment Options</h4>
                    <div className="btns-groups2">
                      <PaymentOptions />
                      <PaymentOptions />
                      <PaymentOptions />
                    </div>
                  </div>
                  <div className="col-sm-12 promocode">
                    <form>
                      <input type="text" className="form-control" id="" value="" placeholder="HAVE OFFER CODE" />
                      <button type="submit" className="btn btn-primary">APPLY</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withRouter(ProductCategory);
