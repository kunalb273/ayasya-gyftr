import React, { Component } from 'react';
import Wrapper from '../wrapper';
import ProductCategory from './category';

// eslint-disable-next-line
export default class CategoryPage extends Component {
  render() {
    return (
      <Wrapper>
        <ProductCategory />
      </Wrapper>
    );
  }
}
