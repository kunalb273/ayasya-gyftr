import { call, put, takeEvery } from 'redux-saga/effects';
import Cookies from 'js-cookie';
import {
    FETCH_USERS_CART,
    DELETE_ITEM_FROM_CART,
} from './constants';
import {
    fetchUsersCart,
    fetchUsersCartSuccess,
    fetchUsersCartFailed,
    deleteFromCartSuccess,
    deleteFromCartFailed,
} from './actions';
import {
    viewUserCart,
    deleteItemFromCart,
} from '../../services';


export function* getCartItems() {
  const token = yield call(Cookies.get, 'Authentication');
  try {
    const resObj = yield call(viewUserCart, token);
    if (resObj.data.data.code === 200) {
      yield put(fetchUsersCartSuccess(resObj.data.data.cartItems));
    } else {
      yield put(fetchUsersCartFailed(resObj));
    }
  } catch (err) {
    yield put(fetchUsersCartFailed('Something Went Wrong'));
  }
}

export function* removeItemFromCart(payload) {
  const cartItemId = payload.payload;
  const token = yield call(Cookies.get, 'Authentication');
  const data = {
    cartItemId, token,
  };
  try {
    const resObj = yield call(deleteItemFromCart, data);
    if (resObj.data.data.code === 200) {
      yield put(deleteFromCartSuccess('Item Removed Successfully'));
      yield (put(fetchUsersCart()));
    } else {
      yield put(deleteFromCartFailed('Something Went Wrong'));
    }
  } catch (err) {
    yield put(deleteFromCartFailed('Something Went Wrong'));
  }
}


// export function* addCartItems() {
//     const token = yield call(Cookies.get, 'Authentication');
//     const productID = 1;
//     const obj = {
//         token,
//         itemId: obj,
//     };
//     try {
//         const resObj = yield call(addItemToCart, token);
//         if (resObj.data.data.code === 200) {
//             yield put(addToCartSuccess(resObj.data));
//         } else {
//             yield put(addToCartFailed(resObj));
//         }
//     } catch (err) {
//         yield put(addToCartFailed("Something Went Wrong"));
//     }
// }


/**
 * Root saga manages watcher lifecycle
 */
export default function* userCart() {
  yield takeEvery(FETCH_USERS_CART, getCartItems);
  yield takeEvery(DELETE_ITEM_FROM_CART, removeItemFromCart);
}
