import { fromJS } from 'immutable';

import {
    ADD_ITEM_TO_CART_SUCCESS,
    ADD_ITEM_TO_CART_FAILED,
    FETCH_USERS_CART_SUCCESS,
    FETCH_USERS_CART_FAILED,
    DELETE_ITEM_FROM_CART_SUCCESS,
    DELETE_ITEM_FROM_CART_FAILED,
} from './constants';

const initialState = fromJS({
  cart: null,
  cartStatus: null,
  addToCartStatus: null,
  deleteFromCartStatus: null,

});

function userCartDetailReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_USERS_CART_SUCCESS:
      return state
                .set('cart', action.payload);
    case FETCH_USERS_CART_FAILED:
      return state
                .set('cartStatus', action.payload);
    case ADD_ITEM_TO_CART_SUCCESS:
      return state
                .set('addToCartStatus', action.payload);
    case ADD_ITEM_TO_CART_FAILED:
      return state
                .set('addToCartStatus', action.payload);
    case DELETE_ITEM_FROM_CART_FAILED:
      return state
                .set('deleteFromCartStatus', action.payload);
    case DELETE_ITEM_FROM_CART_SUCCESS:
      return state
                .set('deleteFromCartStatus', action.payload);
    default:
      return state;
  }
}

export default userCartDetailReducer;
