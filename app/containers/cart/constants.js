export const FETCH_USERS_CART = 'gyftr/userAccountPage/FETCH_USERS_CART';
export const FETCH_USERS_CART_SUCCESS = 'gyftr/userAccountPage/FETCH_USERS_CART_SUCCESS';
export const FETCH_USERS_CART_FAILED = 'gyftr/userAccountPage/FETCH_USERS_CART_FAILED';

export const ADD_ITEM_TO_CART = 'gyftr/userAccountPage/ADD_ITEM_TO_CART';
export const ADD_ITEM_TO_CART_SUCCESS = 'gyftr/userAccountPage/ADD_ITEM_TO_CART_SUCCESS';
export const ADD_ITEM_TO_CART_FAILED = 'gyftr/userAccountPage/ADD_ITEM_TO_CART_FAILED';

export const DELETE_ITEM_FROM_CART = 'gyftr/userAccountPage/DELETE_ITEM_FROM_CART';
export const DELETE_ITEM_FROM_CART_SUCCESS = 'gyftr/userAccountPage/DELETE_ITEM_FROM_CART_SUCCESS';
export const DELETE_ITEM_FROM_CART_FAILED = 'gyftr/userAccountPage/DELETE_ITEM_FROM_CART_FAILED';
