import {
    FETCH_USERS_CART,
    FETCH_USERS_CART_SUCCESS,
    FETCH_USERS_CART_FAILED,
    ADD_ITEM_TO_CART,
    ADD_ITEM_TO_CART_SUCCESS,
    ADD_ITEM_TO_CART_FAILED,
    DELETE_ITEM_FROM_CART,
    DELETE_ITEM_FROM_CART_SUCCESS,
    DELETE_ITEM_FROM_CART_FAILED,
} from './constants';


export function fetchUsersCart(payload) {
  return {
    type: FETCH_USERS_CART,
    payload,
  };
}

export function fetchUsersCartSuccess(payload) {
  return {
    type: FETCH_USERS_CART_SUCCESS,
    payload,
  };
}

export function fetchUsersCartFailed(payload) {
  return {
    type: FETCH_USERS_CART_FAILED,
    payload,
  };
}

export function addToCart(payload) {
  return {
    type: ADD_ITEM_TO_CART,
    payload,
  };
}

export function addToCartSuccess(payload) {
  return {
    type: ADD_ITEM_TO_CART_SUCCESS,
    payload,
  };
}

export function addToCartFailed(payload) {
  return {
    type: ADD_ITEM_TO_CART_FAILED,
    payload,
  };
}

export function deleteFromCart(payload) {
  return {
    type: DELETE_ITEM_FROM_CART,
    payload,
  };
}

export function deleteFromCartSuccess(payload) {
  return {
    type: DELETE_ITEM_FROM_CART_SUCCESS,
    payload,
  };
}

export function deleteFromCartFailed(payload) {
  return {
    type: DELETE_ITEM_FROM_CART_FAILED,
    payload,
  };
}

