import { createSelector } from 'reselect';

const selectUserCart = (state) => state.get('userCart');


const makeSelectUserCart = () => createSelector(
    selectUserCart,
    (state) => state.get('cart')
);

const makeSelectUserCartStatus = () => createSelector(
    selectUserCart,
    (state) => state.get('cartStatus')
);

const makeSelectUserAddToCartStatus = () => createSelector(
    selectUserCart,
    (state) => state.get('addToCartStatus')
);

const makeSelectUserDeleteFromCartStatus = () => createSelector(
    selectUserCart,
    (state) => state.get('deleteFromCartStatus')
);

export {
    makeSelectUserCart,
    makeSelectUserCartStatus,
    makeSelectUserAddToCartStatus,
    makeSelectUserDeleteFromCartStatus,
};
