import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
import {
  fetchUsersCart,
  addToCart,
  deleteFromCart,
} from './actions';
import {
  makeSelectUserCart,
  makeSelectUserCartStatus,
  makeSelectUserAddToCartStatus,
  makeSelectUserDeleteFromCartStatus,
} from './selectors';
import saga from './saga';
import CategroyThumb from '../../components/category-banner';
import CartAddThumb from '../../styles/images/happy-birthday.png';
import rupeeIcon from '../../styles/images/inr.svg';

// eslint-disable-next-line
class Cart extends Component {

  componentWillMount() {
    this.props.fetchUsersCart();
  }
  render() {
    return (
      <div>
        <CategroyThumb />
        <section className="row middle-section">
          <div className="container">
            <div className="row cartArya">
              <div className="col-sm-12 mb-4">
                <div className="row">
                  <div className="col-6 text-left">
                    <a href="test" className="btn btn-outline-secondary">CONTINUE SHOPPING</a>
                  </div>
                  <div className="col-6 text-right">
                    <a href="test" data-toggle="modal" data-target="#GuestLogin" className="btn btn-secondary">CHECKOUT</a>
                  </div>
                </div>
              </div>
              {/*  */}
              <div className="col-sm-12 mb-3">
                <div className="row align-items-center addressOnCart">
                  <div className="col-sm-6 col-lg-4 order-last1">
                    <img id="changeMe" src={CartAddThumb} className="w-100" alt="test" />
                    <div><a className="link-Edit" title="Edit" href="test">Edit</a></div>
                  </div>
                  <div className="col-sm-6 col-lg-8">
                    <div className="box">
                      <address>
                        <h5>DELIVERY ADDRESS</h5>
                        <p>
                          <span>Mr. Zeyaul Haque </span><br />
                          E: zeyaul@jaywalker.io<br />
                          T: 9910448494<br />
                          Delivered On: 23 Nov, 2017<br />
                        </p>
                        <div><a className="link-Edit" title="Change Address" href="test" data-toggle="modal" data-target="#AddressBook">Change Address</a></div>
                      </address>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-12">
                <div className="table-responsive">
                  {this.props.cartItems !== null ?
                    <table className="table cart-table">
                      <thead>
                        <tr className="thead">
                          <th>Brand</th>
                          <th className="description">Denomination</th>
                          <th className="quantity">Units</th>
                          <th>Promo Applied</th>
                          <th className="price" id="priceHeader">Savings</th>
                          <th className="price" id="priceHeader">Total</th>
                          <th className="delete">Delete</th>
                        </tr>
                      </thead>
                      <tbody>

                        {this.props.cartItems.map((item) => (
                          <tr key={item.cartItemId}>
                            <td>{item.product.product_name}</td>
                            <td className="description"><span>{item.product.mrp}</span></td>
                            <td className="quantity">
                              <div className="define-quantity">
                                <input type="text" name="quantity" value="1" className="qty" />
                                <span className="inc button">+</span><span className="dec button"></span>
                              </div>
                            </td>
                            <td>{((item.product.mrp - item.product.selling_price) / item.product.mrp) * 100}% Off</td>
                            <td className="price" id="priceHeader"><span>{item.product.mrp - item.product.selling_price}</span></td>
                            <td className="price" id="priceHeader"><span>{item.product.mrp - item.product.selling_price}</span></td>
                            <td className="delete"><div role="presentation" className="removeItem" onClick={() => this.props.deleteFromCart(item.cartItemId)}><i className="icon"></i> remove</div></td>
                          </tr>
                          ))}
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colSpan="5">Total Products</td>
                          <td colSpan="2" className="description">{this.props.cartItems.length}</td>
                        </tr>
                        <tr>
                          <td colSpan="5">Savings:</td>
                          <td colSpan="2" className="description"><img src={rupeeIcon} style={{ height: '12px' }} alt="test" /> 155</td>
                        </tr>
                        <tr className="total-pay">
                          <td colSpan="5">Total</td>
                          <td colSpan="2" className="description"><img src={rupeeIcon} style={{ height: '25px' }} alt="test" />155</td>
                        </tr>
                      </tfoot>
                    </table>
                    : null
                  }
                </div>
              </div>
              {/*  */}
              <div className="col-sm-12 mb-4">
                <div className="row">
                  <div className="col-6 text-left">
                    <a href="test" className="btn btn-outline-secondary">CONTINUE SHOPPING</a>
                  </div>
                  <div className="col-6 text-right">
                    <a href="test" data-toggle="modal" data-target="#GuestLogin" className="btn btn-secondary">CHECKOUT</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="row recently-viewed br-b-1 br-t-1">
          <div className="container">
            <div className="row">
              <div className="col-sm-12 text-center">
                <h3>Recently Viewed</h3>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export function mapDispatchToProps(dispatch) {
  return {
    fetchUsersCart: () => {
      dispatch(fetchUsersCart());
    },
    addToCart: (payload) => {
      dispatch(addToCart(payload));
    },
    deleteFromCart: (payload) => {
      dispatch(deleteFromCart(payload));
    },
  };
}

const mapStateToProps = createStructuredSelector({
  cartItems: makeSelectUserCart(),
  cartStatus: makeSelectUserCartStatus(),
  addToCartStatus: makeSelectUserAddToCartStatus(),
  deleteFromCart: makeSelectUserDeleteFromCartStatus(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'userCart', saga });

Cart.propTypes = {
  cartItems: PropTypes.array,
  deleteFromCart: PropTypes.func,
  fetchUsersCart: PropTypes.func,
};


export default compose(
  withSaga,
  withConnect,
)(Cart);
