import React, { Component } from 'react';
import Wrapper from '../wrapper';
import Cart from './cart';

// eslint-disable-next-line
export default class CartPage extends Component {
  render() {
    return (
      <Wrapper>
        <Cart />
      </Wrapper>
    );
  }
}
