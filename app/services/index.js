import axios from 'axios';
import qs from 'qs';
import {
  homePageSliderImages,
  topNavProductList,
  brandDetails,
  brandProductDetails,
  homePageStatic,
  userLogin,
  userLogOut,
  userSignUp,
  wishlist,
  getwishlist,
  viewCart,
  addToCart,
  deleteFromCart,
} from '../utils/endpoints';

// Fetch Images for home page Carousel for non logged in users
export async function fetchHomePageSliderImages() {
  const Response = await axios.get(`${homePageSliderImages}`);
  return Response;
}

// Fetch Top Navigation List
export async function fetchTopNavList() {
  const Response = await axios.get(`${topNavProductList}`);
  return Response;
}

// Fetch Brand Page Details
export async function fetchBrandPageDetails(payload) {
  const Response = await axios.get(`${brandDetails}${payload}`);
  return Response.data.data;
}

// Fetch Brand Page Product Details
export async function fetchBrandPageProductDetails(payload) {
  const Response = await axios.get(`${brandProductDetails}${payload}`);
  return Response;
}

// Fetch Static Contents Of Home Page
export async function fetchHomePageStaticContent() {
  const Response = await axios.get(`${homePageStatic}`);
  return Response;
}

// Login User
export async function logInUser(data) {
  const Response = await axios.post(userLogin, qs.stringify({ mobile: data.userName, password: data.password }), { headers: { 'Content-type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// Logout User
export async function logoutUser(token) {
  const Response = await axios.get(userLogOut, { headers: { token: `${token}` } });
  return Response;
}

// User Register
export async function signupUser(details) {
  const Response = await axios.post(userSignUp, qs.stringify({ full_name: `${details.firstName} ${details.lastName}`, mobile: details.mobile, password: details.password }), { headers: { device: 'desktop', platform: 'web', 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// Add Items To Users Wishlist
export async function addToWishlist(details) {
  const Response = await axios.post(wishlist, qs.stringify({ product_id: details.itemId }), { headers: { token: details.token, 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// Fetch Users Wishlist
export async function fetchWishlist(details) {
  const Response = await axios.get(getwishlist, { headers: { token: details.token, 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// view Cart Data
export async function viewUserCart(details) {
  const Response = await axios.get(viewCart, { headers: { token: details, 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// Add Item To Cart
export async function addItemToCart(details) {
  const Response = await axios.get(addToCart, qs.stringify({ product_id: details.itemId }), { headers: { token: details.token, 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}

// Delete Item From Cart
export async function deleteItemFromCart(data) {
  const Response = await axios.get(`${deleteFromCart}${data.cartItemId}`, { headers: { token: data.token, 'Content-Type': 'application/x-www-form-urlencoded' } });
  return Response;
}
