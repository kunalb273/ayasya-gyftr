  /* eslint-disable */
var owl = $('.owl-carousel');
owl.owlCarousel({
	loop:false,
	nav:false,
	dots: false,
	margin:25,
	smartSpeed:1000,
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},            
		768:{
			items:4
		},
		992:{
			items:5
		},
		1200:{
			items:6
		}
	}
});
$( document ).ready(function() {
	var rangeSlider = function(){
	  var slider = $('.range-slider'),
		  range = $('.range-slider__range'),
		  value = $('.range-slider__value');
	  slider.each(function(){
		value.each(function(){
		  var value = $(this).prev().attr('value');
		  $(this).html(value);
		});

		range.on('input', function(){
		  $(this).next(value).html(this.value);
		});
	  });
	};
	rangeSlider();
});

$("#btn1-search, #btn2-search").click(function(){
	$(".search-box").fadeIn();
	$("body").addClass("modal-open");
	$("html, body").animate({ scrollTop: 0 }, 500);
});
$(".search-box #btn-search-close, .storeLocaterIcon").click(function(){
	$(".search-box").fadeOut();
	$("body").removeClass("modal-open");
});
$( document ).on( 'keydown', function ( e ) {
    if ( e.keyCode === 27 ) { // ESC
        $(".search-box").fadeOut();
		$("body").removeClass("modal-open");
    }
});

$(".storeLocaterIcon, #btn1-StoreLocater").click(function(){
	$(".store-locater-box").fadeIn();
	$("body").addClass("modal-open");
	$("html, body").animate({ scrollTop: 0 }, 500);
});
$(".locater-close").click(function(){
	$(".store-locater-box").fadeOut();
	$("body").removeClass("modal-open");
});
$( document ).on( 'keydown', function ( e ) {
    if ( e.keyCode === 27 ) { // ESC
        $(".store-locater-box").fadeOut();
        $("body").removeClass("modal-open");
    }
});

/* Nav */
$(document).ready(function(){
    $("#hamburger-icon").click(function(){
        $("body").toggleClass("open-nav");
    });
	$("#btn2-search, button[data-target='#voucherDetail']").click(function(){
        $("body").removeClass("open-nav");
    });
	$(".nav-back span").click(function(){
        $(".subMenu").removeClass("openSub");
    });
	$(".parent a[data-related='title_1']").click(function(){
        $("#categories").addClass("openSub");
		 return false;
    });
});



/* Nav Count */
$(function() {
  $(".define-quantity").append('<span class="inc button">+</span><span class="dec button">-</span>');
  $(".button").on("click", function() {
    var $button = $(this);
    var oldValue = $button.parent().find("input").val();
    if ($button.text() == "+") {
  	  var newVal = parseFloat(oldValue) + 1;
  	} else {
	   // Don't allow decrementing below zero
      if (oldValue > 0) {
        var newVal = parseFloat(oldValue) - 1;
	    } else {
        newVal = 0;
      }
	  }
    $button.parent().find("input").val(newVal);
  });
});



var options = [];
$( '.dropdown-menu div.custom-control' ).on( 'click', function( event ) {
   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;
   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }
   $( event.target ).blur();
   console.log( options );
   return false;
});

function passwordChanged()
{
    if($('#ChangaPsaword').is(":checked"))   
        $(".password-Hide").slideDown();
    else
        $(".password-Hide").slideUp();
};

$("#addressSearchArya button").click(function(){
    $("#addressTableArya").slideDown();
    $("#addressSearchArya").slideUp();
});


$(function () {
	$("input[name='GyftingRecipient']").click(function () {
		if ($("#Gyfting").is(":checked")) {
			$("#GyftingRecipientBox1").slideDown();
		} else {
			$("#GyftingRecipientBox1").slideUp();
		}
	});
});

$('.preview').on('click',  function() {
    $('#changeMe').prop('src', this.src);
});

$(".myAccount-left h4").click(function(){
    $(this).toggleClass("openMEnu");
    $(".myAccount-arya ul.nav").slideToggle();
});


$(".Entar_Mobile_Section button").click(function(){
    $(".Login_box_Section").fadeIn();
    $(".Entar_Mobile_Section").hide();
});
$(".Login_box_Section .form-text a").click(function(){
    $(".Forgot_box_Section").fadeIn();
    $(".Login_box_Section").hide();
});
$(".Login_box_Section .reg").click(function(){
    $(".Enroll_box_Section").fadeIn();
    $(".Login_box_Section").hide();
});
$(".Forgot_box_Section button").click(function(){
    $(".Login_box_Section").fadeIn();
    $(".Forgot_box_Section").hide();
});
$(".Enroll_box_Section button").click(function(){
    $(".Enroll_OTP_Section").fadeIn();
    $(".Enroll_box_Section").hide();
});

$( function() {
    $( "#EnrollDOB" ).datepicker({
	dateFormat: "dd-mm-yy",
	changeYear: true,
	changeMonth: true,
	maxDate: "-18y"
   });
});

$( function() {
    $( "#DeliveryDate" ).datepicker({
	dateFormat: "dd-mm-yy",
	minDate: "0"
   });
});

$(window).scroll(function() {
  var sticky = $('.headerContainer'),
    scroll = $(window).scrollTop();
   
	  if (scroll >= 160) { 
		sticky.addClass('fixed-top'); }
	  else { 
	   sticky.removeClass('fixed-top');

	}
});


    
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
    

    
	

/* (function ($) {
    $(document).ready(function () {
        if ($(window).width() < 768) {
           $( "#desktop" ).appendTo( ".leftSearch" );
        }
    });
})(jQuery); */