export const topNavProductList = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/categories/topnavigation';
export const homePageSliderImages = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/banner-masters/home';
export const brandDetails = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/brands';
export const brandProductDetails = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/brands/products';
export const homePageStatic = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/static-contents/home';
export const userLogin = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/users/login';
export const userLogOut = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/users/logout';
export const userSignUp = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/users/register';
export const wishlist = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/wishlists/add';
export const getwishlist = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/wishlists/wishlistItems';

export const viewCart = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/cartItems';
export const addToCart = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/carts/add';
export const deleteFromCart = 'http://ec2-13-126-104-7.ap-south-1.compute.amazonaws.com/gyftr/api/v1/cartItems/delete/';

